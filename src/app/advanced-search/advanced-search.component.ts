import { Component, OnInit, AfterViewInit, AfterViewChecked, ChangeDetectorRef, OnDestroy, Optional } from '@angular/core';
import { ConfigService } from 'app/services/config.service';
import { CareerSearchService } from 'app/services/career-search.service';
import { SearchListService } from 'app/services/search-list.service';
import { AdvancedSearchService } from 'app/services/advanced-search.service';
import { UserService} from 'app/services/user.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { FavoritesService } from 'app/services/favorites.service';
import {GoogleAnalyticsService} from 'app/services/google-analytics.service';
import { SaveSearchComponent } from 'app/core/dialogs/save-search/save-search.component';
import { Title, Meta } from '@angular/platform-browser';
import $ from 'jquery'
declare var $: $

export interface Option {
  value: string;
  displayValue: string;
}

@Component({
  selector: 'app-advanced-search',
  templateUrl: './advanced-search.component.html',
  styleUrls: ['./advanced-search.component.scss']
})
export class AdvancedSearchComponent implements OnInit, AfterViewInit, AfterViewChecked, OnDestroy {
  navigationSubscription;
  documentInitialized = false;
  viewLess = false;
  searchList: any;
  displayList: any;
  doubleColumnList: any;
  filteredList: any;
  keyName = 'searchCriteria';
  filteredApplied = false;
  filterProcessing = false;
  favoritesCount = 0;
  myStyle =  {'cursor': 'pointer'};
  disabled = false;
  listSize = 10;
  halfListSize = 5;
  keyword = '';
  currentSearchCriteria: any;
  savedSearches: any;
  mobileDisplayedCareers: any;
  careerCluster: any;
  currentCareer: number;
  currentIndex: number;
  lastCareerOnPage: number;
  numberOfCareers: number;
  imageUrl: string;
  displayTabs = false;
  mostImportant = false;
  moderateImportant = false;
  leastImportant = false;
  routeParam: String;
  advancedSearchListInterval: any;
  selectedValue: any;
  serviceCount = 5;
  typeCount = 2;
  interestCount = 6;
  fcCount = 1;
  typeSelected = undefined;
  interestsSelected = [];
  menuOptions: Option[] = [
    {value: 'name', displayValue: '-- Select Skill Importance (Optional) --'},
    {value: 'math', displayValue: 'Math'},
    {value: 'verbal', displayValue: 'Verbal'},
    {value: 'science', displayValue: 'Science / Technology'}
  ];
 // Service checkbox values.
  serviceCheckboxValue = [{
    value: 'army',
    id: 'service1',
    text: 'Army'
  }, {
    value: 'marines',
    id: 'service2',
    text: 'Marine Corps'
  }, {
    value: 'airForce',
    id: 'service3',
    text: 'Air Force'
  }, {
    value: 'navy',
    id: 'service4',
    text: 'Navy'
  }, {
    value: 'coastGuard',
    id: 'service5',
    text: 'Coast Guard'
  }];

   // Type checkbox values.
   typeCheckboxValue = [{
    value: 'enlisted',
    id: 'type1',
    text: 'Enlisted'
  }, {
    value: 'officer',
    id: 'type2',
    text: 'Officer'
  }, 
];

   // Interest checkbox values.
   interestCheckboxValue = [{
    value: 'realistic',
    id: 'interest1',
    text: 'Realistic'
  }, {
    value: 'investigative',
    id: 'interest2',
    text: 'Investigative'
  }, {
    value: 'artistic',
    id: 'interest3',
    text: 'Artistic'
  }, {
    value: 'social',
    id: 'interest4',
    text: 'Social'
  }, {
    value: 'enterprising',
    id: 'interest5',
    text: 'Enterprising'
  }, {
    value: 'conventional',
    id: 'interest6',
    text: 'Conventional'
  }];

   // Featured Category checkbox values.
   fcCheckboxValue = [{
    value: 'hotJob',
    id: 'fc1',
    text: 'Hot Job'
  }];
  allSearchCriterial = [
    this.serviceCheckboxValue,
    this.typeCheckboxValue,
    this.interestCheckboxValue,
    this.fcCheckboxValue
  ];


  constructor(
    private _config: ConfigService,
    @Optional() private cdr: ChangeDetectorRef,
    private _userService: UserService,
    private _searchService: SearchListService,
    private _advancedSearch: AdvancedSearchService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _meta: Meta,
    private _titleTag: Title,
    private _favoriteService: FavoritesService,
    private _spinner: NgxSpinnerService,
    private _careerSearchService: CareerSearchService,
    private _googleAnalyticsService: GoogleAnalyticsService
  ) {
    this.imageUrl = this._config.getImageUrl();
      // Subscribe to router event Navigation End so can refresh data after log in
      this.navigationSubscription = this._router.events.subscribe(event => {
        if (event instanceof NavigationEnd && event.urlAfterRedirects.indexOf('/advanced-search') > -1) {
          this._route.params.subscribe(res => {
            this.routeParam = res.filter;
          });
          this.loadData();
        }
      });
  }

  ngOnInit() {
    this._spinner.show();
    this._route.params.subscribe(res => {
      this.routeParam = res.filter;
    });

    if (this.isLoggedIn()) {
    this._userService.getSavedSearches().subscribe(data => {
      this.savedSearches = data;
    });
  }

    this._careerSearchService.getCareerCluster().subscribe(data => {
      this.careerCluster = data;
      this._careerSearchService.setCareerCluster(data);
    });

    this._route.params.subscribe(res => {
      this.routeParam = res.filter;
    });

    // if (this._searchService.currentSearchCriteria) {
    //   this.currentSearchCriteria = this._searchService.currentSearchCriteria;
    //   this._searchService.currentSearchCriteria = null;
    // }
    this._titleTag.setTitle('Advanced Search | Careers in the Military');
    this._meta.updateTag({name: 'description', content: 'Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests.'});

    this._meta.updateTag({ property: 'og:title', content: 'Advanced Search | Careers in the Military' });
    this._meta.updateTag({ property: 'og:description', content: 'Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests.' });
    this._meta.updateTag({ property: 'og:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });
    this._meta.updateTag({ name: 'twitter:title', content: 'Advanced Search | Careers in the Military' });
    this._meta.updateTag({ name: 'twitter:description', content: 'Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests.' });
    this._meta.updateTag({ name: 'twitter:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });

 }

 ngAfterViewInit() {
    this.documentInitialized = true;
    window.scroll(0, 0);
    this.initializeSearchList();
    $("#sticky").sticky({topSpacing:0, bottomSpacing:600});
  }

  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }

  loadData() {
    this._spinner.show();
    this.filterProcessing = false;

    const searchObject = this._searchService.getSearchList();
    const self = this;
    if (!searchObject) {
      if (!this.advancedSearchListInterval) {
          this.advancedSearchListInterval = setInterval(function () {
          if (!self._searchService.getProcessingSearchList()) {
            clearInterval(self.advancedSearchListInterval);
            self.searchList = self._searchService.searchListObject;
            self.favoritesCount = self._userService.favoritesListObject ? self._userService.favoritesListObject.length : 0;
            self.setSearchList();
            self.initializeSearchList();
          }
        }, 200);
      }
    } else {
      this.searchList = searchObject;
      this.favoritesCount = this._userService.favoritesListObject.length;
      this.setSearchList();
      this.initializeSearchList();
    }
  }

  initializeSearchList() {
    // Make sure all document elements exist and searchList exists
    if (this.documentInitialized && this.searchList) {
      let searchSet = false;
      // Load search from route parameter
      if (this.routeParam) {
        switch (this.routeParam) {
          case ('hotJobs'):
            (this.getSearchCriteriaElement('hotJob')).checked = true;
            this.setSearchListFilters();
            searchSet = true;
            break;
          case ('officer'):
            (this.getSearchCriteriaElement('officer')).checked = true;
            this.setSearchListFilters();
            searchSet = true;
            break;
          case ('enlisted'):
            (this.getSearchCriteriaElement('enlisted')).checked = true;
            this.setSearchListFilters();
            searchSet = true;
            break;
        }
      }
    // Load search from Profile page
      if (!searchSet && this._searchService.currentSearchCriteria) {
        this.currentSearchCriteria = this._searchService.currentSearchCriteria;
        this._searchService.currentSearchCriteria = null;
        this.loadExistingSearch();
      // Load search from Last Choices
      } else if (!searchSet && sessionStorage.getItem(this.keyName)) {
        this.currentSearchCriteria = JSON.parse(sessionStorage.getItem(this.keyName));
        this.loadExistingSearch();
      }
      if (this.currentSearchCriteria) {
        this.setSearchListFilters();
      } else {
        this.setDisplayList();
      }
      this._spinner.hide();
    }
  }

  setSearchList() {
    this.numberOfCareers = this.searchList.length;

    this.currentCareer = 1;
    this.lastCareerOnPage = this.currentCareer + 9 <= this.numberOfCareers ? this.currentCareer + 9 : this.numberOfCareers;
    this.selectedValue = 'name';
    this.setDisplayList();
  }

  serviceClicked(event) {
    const inputElement = (event.target || event.srcElement || event.currentTarget) as HTMLInputElement;
    const code = inputElement.value;
    const path = this._route.url;
    if (code === 'N') {
      this.ga(path + '#CITM_ADVSEARCH_FILTER_SERVICE_NAVY');
    }

    if (code === 'A') {
      this.ga(path + '#CITM_ADVSEARCH_FILTER_SERVICE_ARMY');
    }

    if (code === 'C') {
      this.ga(path + '#CITM_ADVSEARCH_FILTER_SERVICE_COAST_GUARD');
    }

    if (code === 'F') {
      this.ga(path + '#CITM_ADVSEARCH_FILTER_SERVICE_AIR_FORCE');
    }

    if (code === 'M') {
      this.ga(path + '#CITM_ADVSEARCH_FILTER_SERVICE_MARINES');
    }
    this.checkboxClicked();
  }

  ga(param) {
    this._googleAnalyticsService.trackClick(param);
  }

    gaSocialShare(socialPlug, value) {
      this._googleAnalyticsService.trackSocialShare(socialPlug, value);
    }

    checkboxClicked() {
      this.setSearchListFilters();
    }

  setSearchListFilters() {
    if (!this.filterProcessing) {

      const itemsSet = this.getCheckedItems();
      if (itemsSet.length < 1 && !this.keyword) {
        this.interestsSelected = [];
        this.typeSelected = undefined;
        this.onClearSearch();
        return;
      }

      this.typeSelected = itemsSet.find(i => 
        i.key[0] === 'enlisted' || i.key[0] === 'officer'
      )

      if (this.typeSelected) {
        this.typeSelected = this.typeSelected['key'][0];
      }

      this.interestsSelected = [];
      itemsSet.forEach(i => {
        if (i.key[0] === 'realistic' ||
        i.key[0] === 'investigative' ||
        i.key[0] === 'artistic' ||
        i.key[0] === 'social' ||
        i.key[0] === 'enterprising' ||
        i.key[0] === 'conventional') {
          this.interestsSelected.push(i.key[0]);
        }
      })

      this.filterProcessing = true;
      this._spinner.show();

      this.currentSearchCriteria = this.getSearchCriteria();
      let tabName = 'None';
      if (this.displayTabs) {
        if (this.mostImportant) {
          tabName = 'Most';
        } else if (this.moderateImportant) {
          tabName = 'Moderate';
        } else if (this.leastImportant) {
          tabName = 'Least';
        }
      }

      this.filteredList = this._advancedSearch.filterList(this.currentSearchCriteria, this.searchList, this.selectedValue, tabName);
      this.filteredApplied = true;
      this.afterFilterSearch();
      this.currentSearchCriteria['show'] = this.viewLess ? 'list' : 'detail';
      this.currentSearchCriteria['skill'] = this.selectedValue;
      this.currentSearchCriteria['displayTabs'] = this.displayTabs;
      this.currentSearchCriteria['tabName'] = this.mostImportant ? 'Most' : this.moderateImportant ? 'Moderate' : 'Least';
      sessionStorage.setItem(this.keyName, JSON.stringify(this.currentSearchCriteria));
 
      this.filterProcessing = false;
    this._spinner.hide();

    }
  }

  loadExistingSearch() {
    for (const key in this.currentSearchCriteria) {
      if (this.currentSearchCriteria[key] === true) {
        if (key !== 'displayTabs'  && key !== 'keyword') {
          (this.getSearchCriteriaElement(key)).checked = true;
        }
      }
    }
    if (this.currentSearchCriteria['keyword']) {
      this.keyword = this.currentSearchCriteria['keyword'];
    }
    this.viewLess = false;
    if (this.currentSearchCriteria.show === 'list') {
      this.viewLess = true;
    }
    if (this.currentSearchCriteria['skill'] !== 'name') {
     this.selectedValue = this.currentSearchCriteria['skill'];
      this.displayTabs = true;
      this.onClickTab(this.currentSearchCriteria.tabName);
      return;
    }
    this.setSearchListFilters();
  }

  isLoggedIn() {
    return this._config.isLoggedIn();
  }

  getSearchCriteriaElement(key) : HTMLInputElement {
    for (let i = 0; i < this.allSearchCriterial.length; i++) {
      const typeArray = this.allSearchCriterial[i];
      for (let x = 0; x < typeArray.length; x++) {
        if (typeArray[x].value === key) {
          const elementId = typeArray[x].id;
          return (document.getElementById(elementId) as HTMLInputElement);
      }
      }
    }
  }

  saveSearch() {
    const itemsSet  =  this.getSearchCriteria();

    this._advancedSearch.saveSearch(itemsSet);
  }

  getSearchCriteria() {
    let itemsSet = { };
    //  Military Service get value
    this.times(this.serviceCount)(i =>
      this.checkedValue('service' + (i + 1), itemsSet)
    );
    //  Type get value
    this.times(this.typeCount)(i =>
      this.checkedValue('type' + (i + 1), itemsSet)
    );
    //  Interest get value
    this.times(this.interestCount)(i =>
      this.checkedValue('interest' + (i + 1), itemsSet)
    );

    //  Featured Category clear checked
    this.times(this.fcCount)(i =>
      this.checkedValue('fc' + (i + 1), itemsSet)
    );
    let searchValue = this.keyword;
    if (!searchValue) {
      searchValue = '';
    }

    itemsSet['keyword'] = searchValue;
    return itemsSet;
  }

  checkedValue(elementId, itemsSet) {
    const elementValue = (document.getElementById(elementId) as HTMLInputElement) .value;
    const  isChecked = (document.getElementById(elementId) as HTMLInputElement) .checked;
    itemsSet[elementValue] = isChecked;
  }

  verifySave() {
    const itemsSet = this.getCheckedItems();
    if (this._advancedSearch.verifySaveSearch(itemsSet, this.savedSearches)) {
      this.saveSearch();
    }
  }

  getCheckedItems() {
        //  Military Service get value
        const itemsSet = [];
        this.times (this.serviceCount) ( i =>
          this.isChecked('service' + (i + 1), itemsSet)
        );
        //  Type get value
        this.times (this.typeCount) ( i =>
          this.isChecked('type' + (i + 1), itemsSet)
        );
        //  Interest get value
        this.times (this.interestCount) ( i =>
          this.isChecked('interest' + (i + 1), itemsSet)
        );

        //  Featured Category clear checked
        this.times (this.fcCount) ( i =>
          this.isChecked('fc' + (i + 1), itemsSet)
        );
        const searchValue = this.keyword;
        if (searchValue) {
          itemsSet.push({
              key: 'keyword',
              value: searchValue
            });
        }
        if (this.selectedValue !== 'name') {
          itemsSet.push({
            key: 'skillView',
            value: this.selectedValue
          });
        }
        return itemsSet;
  }

  isChecked(elementId, list) {
    const element = (document.getElementById(elementId) as HTMLInputElement);
    if (element.checked) {
      const value = element.value;
      list.push({
        key: [value],
        value: element.checked
      });
    }
  }

  onNextSet() {
    if (this.lastCareerOnPage + 1 <= this.numberOfCareers) {
      this.currentCareer = this.lastCareerOnPage + 1;
      this.lastCareerOnPage = this.currentCareer + (this.listSize - 1) <=
      this.numberOfCareers ? this.currentCareer + (this.listSize - 1) : this.numberOfCareers ;
      this.setDisplayList();
    }
  }

  onPreviousSet() {
    if (this.numberOfCareers > 0) {
      this.currentCareer = this.currentCareer - this.listSize > 0 ? this.currentCareer - this.listSize : 1;
      this.lastCareerOnPage = this.currentCareer + (this.listSize - 1) <=
      this.numberOfCareers ? this.currentCareer + (this.listSize - 1) : this.numberOfCareers ;
      this.setDisplayList();
    }
  }

  afterFilterSearch() {
    this.currentCareer = this.filteredList.length > 0 ? 1 : 0;
    this.setDisplayList();
    this.lastCareerOnPage = this.currentCareer + 9 <= this.numberOfCareers ? this.currentCareer + 9 : this.numberOfCareers;
  }

  times = n => f => {
    const iter = i => {
      if (i === n) {return; }
      f (i);
      iter (i + 1);
    };
    return iter (0);
  }

  setDisplayList() {
    this.currentIndex = this.currentCareer - 1;
    let fullList;
    if (this.filteredApplied) {
      fullList = this.filteredList;
      this.numberOfCareers = this.filteredList.length;
    } else {
      fullList = this.searchList;
    }

    fullList.sort(this.compare);

    const buildList  = [] ;
    if (this.currentCareer > 0) {
    const numberOfTimes = (this.currentIndex + this.listSize) < this.numberOfCareers ?
        this.listSize : this.numberOfCareers - this.currentIndex;
    this.times (numberOfTimes) ( i =>
      buildList.push(fullList[i + this.currentIndex])
    );
  }
    this.displayList = buildList;
  }

  compare(a, b) {
    if ( a.title < b.title ){
      return -1;
    }
    if ( a.title > b.title ){
      return 1;
    }
    return 0;
  }

  onChangeView(type) {
    if (type === 'Detail') {
      this.viewLess = false;
    } else if (type === 'List') {
      this.viewLess = true;
    }
  }

  getDisplayValue() {
    return (this.menuOptions.find(obj => obj.value === this.selectedValue)).displayValue;
  }

  isSelected(lineValue) {
    if (this.selectedValue === lineValue) {
      return true;
    }
    return false;
  }

  onSkillChange() {
    if (this.selectedValue && this.selectedValue !== 'name') {
     this.displayTabs = true;
     this.onClickTab('Most');
    } else {
      this.displayTabs = false;
      this.setSearchListFilters();
    }
  }


  onFavoriteClick(item) {
    if (this.disabled) {
      return;
    }
    this.disabled = true;
    this.myStyle =  {'cursor': 'wait'};
    const favorite = item.favorited;
    return this._favoriteService.onFavoriteClick(item)
    .then (() => {
      this.favoritesCount = this._userService.favoritesListObject.length;
      this.disabled = false;
      this.myStyle =  {'cursor': 'pointer'};
      return;
    })
    .catch(() => {
      this.disabled = false;
      this.myStyle =  {'cursor': 'pointer'};
      return;
    });
  }

  onClickTab(tab) {
    this.mostImportant = false;
    this.moderateImportant = false;
    this.leastImportant = false;

    // Set the filters on the searchList
    switch (tab) {
      case ('Most'):
        this.mostImportant = true;
        this.setSearchListFilters();
        break;
        case ('Moderate'):
        this.moderateImportant = true;
        this.setSearchListFilters();
        break;
        case ('Least'):
        this.leastImportant = true;
        this.setSearchListFilters();
        break;
    }
  }

  onLoadMore() {
    this.listSize += 10;
    this.halfListSize = this.listSize / 2;
    this.lastCareerOnPage = this.currentCareer + (this.listSize - 1) <=
     this.numberOfCareers ? this.currentCareer + (this.listSize - 1) : this.numberOfCareers ;
    this.setDisplayList();
  }

  onClearSearch() {
    this.displayTabs = false;
    this.selectedValue = 'name';
    this.filteredApplied = false;
    this.keyword = '';
    this.selectedValue = 'name';
    this.viewLess = false;
    this.displayTabs = false;


    //  Military Service clear checked
    this.times (this.serviceCount) ( i =>
    this.clearElements('service' + (i + 1))
    );
    //  Type clear checked
    this.times (this.typeCount) ( i =>
      this.clearElements('type' + (i + 1))
    );
     //  Interest clear checked
     this.times (this.interestCount) ( i =>
      this.clearElements('interest' + (i + 1))
    );
    //  Featured Category clear checked
    this.times(this.fcCount)(i =>
      this.clearElements('fc' + (i + 1))
    );

    sessionStorage.removeItem(this.keyName);
    this.currentSearchCriteria = null;
    this.setSearchList();
  }

  clearElements(elementId) {
    (document.getElementById(elementId) as HTMLInputElement).checked = false;
  }

  scrollToElement(elementName) {
    if (this.displayTabs && elementName === 'searchResults') {
      // tslint:disable-next-line:no-unused-expression
      elementName === 'searchTabs';
    }
    const el = document.getElementById(elementName);
    el.scrollIntoView();
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }
}
