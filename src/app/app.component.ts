import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

declare var ga: Function;

@Component( {
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
} )
export class AppComponent {
    title = 'app';
    public loading = false;

    constructor(
        public router: Router
        ) {
        this.router.events.subscribe( event => {
            // console.log('inside subscribe');
            if ( event instanceof NavigationEnd ) {
                // console.log('inside NavigationEnd');
                ga( 'set', 'page', event.urlAfterRedirects );
                ga( 'send', 'pageview' );
            }
        } );
    }
}
