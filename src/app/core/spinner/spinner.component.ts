import { Component, OnInit, OnDestroy, ViewEncapsulation, CUSTOM_ELEMENTS_SCHEMA, AfterViewInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SpinnerComponent implements OnInit, OnDestroy {

  loading = false;

  constructor(
    private _router: Router,
  ) {
    this._router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.loading = true;
      } else if ( event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) {
        this.loading = false;
      }
    }, () => {
      this.loading = false;
    });
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.loading = false;
  }

}
