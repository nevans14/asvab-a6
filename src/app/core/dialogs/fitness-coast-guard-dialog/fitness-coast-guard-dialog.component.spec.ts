import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitnessCoastGuardDialogComponent } from './fitness-coast-guard-dialog.component';

describe('FitnessCoastGuardDialogComponent', () => {
  let component: FitnessCoastGuardDialogComponent;
  let fixture: ComponentFixture<FitnessCoastGuardDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitnessCoastGuardDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitnessCoastGuardDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
