import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-find-recruiter-dialog',
  templateUrl: './find-recruiter-dialog.component.html',
  styleUrls: ['./find-recruiter-dialog.component.scss']
})
export class FindRecruiterDialogComponent implements OnInit {
  serviceContacts: any;
  constructor(
    public dialogRef: MatDialogRef<FindRecruiterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) {}

  ngOnInit() {
    this.serviceContacts = this.data.serviceContacts;
  }

  /**
   * Find associated image icon.
   */
  imageTitle() {
    var svcCode = this.serviceContacts.serviceCode;

    if (svcCode == 'A') {
      return 'army';
    } else if (svcCode == 'F') {
      return 'air-force';
    } else if (svcCode == 'N') {
      return 'natl-guard';
    } else if (svcCode == 'C') {
      return 'coast-guard';
    } else if (svcCode == 'N') {
      return 'navy';
    } else if (svcCode == 'M') {
      return 'marines';
    }
  }

  close() {
    this.dialogRef.close();
  }
}
