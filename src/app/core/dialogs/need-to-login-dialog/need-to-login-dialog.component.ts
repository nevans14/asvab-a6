import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatDialog } from '@angular/material';
import { LoginDialogComponent } from 'app/core/dialogs/login-dialog/login-dialog.component';


@Component({
    selector: 'app-need-to-login-dialog',
    templateUrl: './need-to-login-dialog.component.html',
    styleUrls: ['./need-to-login-dialog.component.scss']
})
export class NeedToLoginDialogComponent implements OnInit {

    constructor(
        private _dialog: MatDialog,
        public dialogRef: MatDialogRef<NeedToLoginDialogComponent>
        ) { }

    ngOnInit() {
    }

    onLogIn() {
        const dialogRef = this._dialog.open(LoginDialogComponent, {
            panelClass: 'custom-dialog',
            hasBackdrop: true,
            disableClose: true
        });
        this.close();
    }

    close() {
        this.dialogRef.close();
    }

}
