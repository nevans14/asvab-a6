import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ContentManagementService } from 'app/services/content-management.service';

@Component({
  selector: 'app-fitness-navy-dialog',
  templateUrl: './fitness-navy-dialog.component.html',
  styleUrls: ['./fitness-navy-dialog.component.scss']
})
export class FitnessNavyDialogComponent implements OnInit, OnDestroy {
  pageHtml;
  page: any;

  constructor(
    private _contentManagementService: ContentManagementService,
    public dialogRef: MatDialogRef<FitnessNavyDialogComponent>
  ) { }


  ngOnInit() {
    this.getPageByName('dialog-fitness-navy');
  }

  /***
   * Get content from DB
   */
  getPageByName(pageName) {
    // Remove leading slash before lookup
    if (pageName.indexOf('/') === 0) {
      pageName = pageName.substring(1);
    }
    console.log('pageName:', pageName);
    this._contentManagementService.getPageByPageName(pageName).subscribe(
      data => {
        this.page = data;
        this.pageHtml = this.page.pageHtml;
      });
  }

  close() {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    (document.getElementById('dir')[0] as Component) = null;
  }
}
