import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { ConfigService } from 'app/services/config.service';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { ReactiveFormsModule, FormGroup, FormBuilder } from '@angular/forms';
import { PathLocationStrategy } from '@angular/common';

@Component({
  selector: 'app-update-user-info',
  templateUrl: './update-user-info.component.html',
  styleUrls: ['./update-user-info.component.scss']
})
export class UpdateUserInfoComponent implements OnInit, AfterViewInit {
  public fileForm: FormGroup;
  userInfo: any;
  userInfoId: number;
  result = false;
  name: string;
  schoolName: string;
  graduationYear: number;
  fileName: string;
  selectedFile: File = null;
  thumbnail: any;
  progress: any;
  error: any;
  roundedSize: number;
  fileUrl: any;
  photo: any;
  showSpinner = false;

  constructor(
    public _dialogRef: MatDialogRef<UpdateUserInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _fb: FormBuilder,
    private _httpClient: HttpClient,
    private _sanitizer: DomSanitizer,
    private _config: ConfigService
  ) { }

  ngOnInit() {
    this.userInfo = {};
    if (this.data.data.photo) {
      this.displayPhoto();
    } else {
      this.photo = 'assets/images/profile.jpg';
    }
    if (this.data) {
      this.name = this.data.data.name;
      this.schoolName = this.data.data.schoolName;
      this.graduationYear = this.data.data.graduationYear;
      this.userInfoId = this.data.data.id;
      } else {
      this.name = '';
      this.schoolName = '';
      this.graduationYear = null;
      this.userInfoId = null;
    }
  }

  ngAfterViewInit() {
  }

  onSelectFile(event) {
    const self = this;
    if (event.target.files.length > 0) {
      this.selectedFile = <File>event.target.files[0];
      this.roundedSize = Math.round((this.selectedFile.size / 1000000) * 10) / 10;

      // set file to src to display
      const reader = new FileReader();
      reader.addEventListener('load', function () {
        self.photo = reader.result;
      }, false);
      reader.readAsDataURL(this.selectedFile);
    }
  }

  displayPhoto() {
    this.fileName = this.data.data.fileName;
    this.photo = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64, ' + this.data.data.photo);
  }

  fileSizeOk() {
    if (this.selectedFile && this.selectedFile.size > 200000) {
      return false;
    }
    return true;
  }

  // Save User Info.
  saveInfo() {
    this.showSpinner = true;
    this.error = null;
    this.userInfo.name = this.name;
    this.userInfo.schoolName = this.schoolName;
    this.userInfo.graduationYear = this.graduationYear;
    if (!this.userInfo.userId) {
      this.userInfo.userId = this._config.getUserId();
    }
    this.userInfo.id = this.userInfoId ? this.userInfoId : 0;

    const fd = new FormData();
    if (this.selectedFile) {
        fd.append('file', this.selectedFile, this.selectedFile.name);
    }
    fd.append('userId', this.userInfo.userId);
    fd.append('id', this.userInfo.id);
    fd.append('name', this.userInfo.name);
    fd.append('schoolName', this.userInfo.schoolName);
    fd.append('graduationYear', this.userInfo.graduationYear);

    const restUrl = this._config.getRestUrl() ;
    const url = restUrl + '/portfolio/upload';
    this._httpClient.post(url,  fd,  {
      reportProgress: true,
      observe:  'events'
    }).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress = Math.round(event.loaded / event.total * 100) ;
      }
      if (event.type === HttpEventType.Response) {
        this.result = true;
        this.showSpinner = false;
        this._dialogRef.close();
      }
     },
    error  => {
      this.showSpinner = false;
      this.error = error;
    },
    ()  => {
      this.showSpinner = false;
    });
  }

  formNotValid() {
    if (!this.fileSizeOk() || !this.name || !this.schoolName ||
     !this.graduationYear || (this.graduationYear < 1940 || this.graduationYear > 3000)) {
      return true;
    }
    return false;
  }

  onCancel() {
    this._dialogRef.close();
  }
}
