import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-rank-dialog',
    templateUrl: './rank-dialog.component.html',
    styleUrls: ['./rank-dialog.component.scss']
})
export class RankDialogComponent implements OnInit {


    rank;
    serviceList = [{
        id: 'A',
        title: 'Army'
        }, {
            id: 'F',
            title: 'Air Force'
        }, {
            id: 'M',
            title: 'Marine'
        }, {
            id: 'N',
            title: 'Navy'
        }, {
            id: 'C',
            title: 'Coast Guard'
        }];
    serviceSelection;
    constructor(public dialogRef: MatDialogRef<RankDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data) { }

    ngOnInit() {
        this.rank = this.data;
    }

    filterRankEnlisted(rank): any[] {
        let rankFiltered = rank.filter(r => r.service == this.serviceSelection && r.rate.match(/E-/));
        return rankFiltered;
    }

    filterRankOfficer(rank): any[] {
        let rankFiltered = rank.filter(r => r.service == this.serviceSelection && r.rate.match(/O-/));
        return rankFiltered;
    }

    filterRankWarrantOfficer(rank): any[] {
        let rankFiltered = rank.filter(r => r.service == this.serviceSelection && r.rate.match(/W-/));
        return rankFiltered;
    }

    close() {
        this.dialogRef.close();
    }

}
