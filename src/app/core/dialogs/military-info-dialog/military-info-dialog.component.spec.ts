import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MilitaryInfoDialogComponent } from './military-info-dialog.component';

describe('MilitaryInfoDialogComponent', () => {
  let component: MilitaryInfoDialogComponent;
  let fixture: ComponentFixture<MilitaryInfoDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MilitaryInfoDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilitaryInfoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
