import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupPlayerComponent } from './popup-player.component';

describe('PopupPlayerComponent', () => {
  let component: PopupPlayerComponent;
  let fixture: ComponentFixture<PopupPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
