import { Component, OnInit, AfterViewInit, AfterViewChecked, ChangeDetectorRef, ViewChild, OnDestroy, Optional } from '@angular/core';
import { PageEvent } from '@angular/material';
import { MatPaginator } from '@angular/material';
import { ConfigService } from 'app/services/config.service';
import { SearchListService } from 'app/services/search-list.service';
import { UserService } from 'app/services/user.service';
import { FavoritesService } from 'app/services/favorites.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Router, NavigationEnd } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import $ from 'jquery'
declare var $: $

@Component( {
    selector: 'app-guided-exploration',
    templateUrl: './guided-exploration.component.html',
    styleUrls: ['./guided-exploration.component.scss']
} )
export class GuidedExplorationComponent implements OnInit, AfterViewInit, AfterViewChecked, OnDestroy {
    navigationSubscription;
    favoritesCount = 0;
    myStyle =  {'cursor': 'pointer'};
    disabled = false;
    // Where we are in the quiz
    currentPage = 1;
    qId = 1;

    searchList: any;
    allJobList: any;
    detailView = false;
    gridView = true;
    guidedSearchListInterval: any;
    description = 'Answer questions based on your preferences. We\'ll make recommendations just for you.';

    /**
     * Interest checkbox selections
     */
    public checkboxInterest = [{
        name: 'Work with my hands and see concrete results',
        code: 'R'
    }, {
        name: 'Test ideas, learn new things, and solve problems',
        code: 'I'
    }, {
        name: 'Use my imagination and create original work',
        code: 'A'
    }, {
        name: 'Help others and build relationships',
        code: 'S'
    }, {
        name: 'Lead others and make decisions',
        code: 'E'
    }, {
        name: 'Organize items and work with details',
        code: 'C'
    }];


    public filterItems = {
        'R': false,
        'I': false,
        'A': false,
        'S': false,
        'E': false,
        'C': false
    };


    /**
     * Work environment checkboxs.
     */
    public checkboxWorkEnvironment = [{
        name: 'Office-like setting',
        code: 1
    }, {
        name: 'Community or retail center',
        code: 2
    }, {
        name: 'Plant or factory',
        code: 3
    }, {
        name: 'On the water or under the sea',
        code: 4
    }, {
        name: 'In the air',
        code: 5
    }, {
        name: 'Warehouse',
        code: 6
    }, {
        name: 'Outdoors',
        code: 7
    }, {
        name: 'Medical facility',
        code: 8
    }, {
        name: 'Construction site',
        code: 9
    }, {
        name: 'Garage or repair shop',
        code: 10
    }, {
        name: 'Laboratory',
        code: 11
    }, {
        name: 'Command center',
        code: 12
    }, {
        name: 'Classroom',
        code: 13
    }, {
        name: 'Enclosed vehicle',
        code: 14
    }];


    /**
     * Work environment checkbox selections.
     */
    public workEnvironmentItems = {
        1: false,
        2: false,
        3: false,
        4: false,
        5: false,
        6: false,
        7: false,
        8: false,
        9: false,
        10: false,
        11: false,
        12: false,
        13: false,
        14: false
    };


    /**
     * Skill checkbox selections.
     */
    public skillItems = [{
        name: 'Working with Numbers  (Math)',
        code: 'M'
    }, {
        name: 'Experimenting & Building (Science/Technical)',
        code: 'S'
    }, {
        name: 'Reading & Writing (Verbal)',
        code: 'V'
    }];


    /**
     * Skill checkbox values.
     */
    public skillCheckBox = {
        'M': false,
        'S': false,
        'V': false
    };

    guidedExplorationCurrentIndex = 0;
    isGuidedExplorationViewResults = false;

    guidedSelection = {
      EnvironmentItems: this.workEnvironmentItems,
      filterItems: this.filterItems,
      skillCheckBox: this.skillCheckBox,
      qId: this.qId,
      viewResults: this.isGuidedExplorationViewResults,
      currentJob: this.guidedExplorationCurrentIndex
    };

    // pagination
    @ViewChild( 'paginator' ) vc: MatPaginator;
    careersLength: number;
    pageIndex = 0;
    pageSize = 6;
    lowValue = 0;
    highValue = 6;
    pageEvent: PageEvent;

    constructor(
        private _config: ConfigService,
        @Optional() private cdr: ChangeDetectorRef,
        private _searchService: SearchListService,
        private _userService: UserService,
        private _favoriteService: FavoritesService,
        private _spinner: NgxSpinnerService,
        private sanitizer: DomSanitizer,
        private _meta: Meta,
        private _titleTag: Title,
        private _router: Router
    ) {       // Subscribe to router event Navigation End so can refresh data after log in
        this.navigationSubscription = this._router.events.subscribe(event => {
          if (event instanceof NavigationEnd && event.urlAfterRedirects.indexOf('/guided-exploration') > -1) {
            this.loadData();
          }
        });
  }

    ngOnInit() {
        this._titleTag.setTitle('Guided Exploration | Careers in the Military');
        this._meta.updateTag({name: 'description', content: this.description});

        this._meta.updateTag({ property: 'og:title', content: 'Guided Exploration | Careers in the Military' });
        this._meta.updateTag({property: 'og:description', content: this.description});
        this._meta.updateTag({ property: 'og:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });
        this._meta.updateTag({ name: 'twitter:title', content: 'Guided Exploration | Careers in the Military' });
        this._meta.updateTag({name: 'twitter:description', content: this.description});
        this._meta.updateTag({ name: 'twitter:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });
    }

    ngAfterViewInit() {
        // Using ChangeDetectorRef gets error in profile page. And using change detection OnPush conflicts with favorite function. So this is an alternative solution trigger a change detection.
        //https://stackoverflow.com/questions/46796299/angular-how-to-use-app-tick-can-anyone-give-me-an-example-of-applicationre
        setTimeout(() =>{});
        window.scroll(0, 0);
    }

    ngAfterViewChecked() {
      this.cdr.detectChanges();
    }

    loadData() {
        this._spinner.show();
        const searchObject = this._searchService.getSearchList();
        const self = this;
        if (!searchObject) {
            if (!this.guidedSearchListInterval) {
                this.guidedSearchListInterval = setInterval(function () {
                    if (!self._searchService.getProcessingSearchList()) {
                        clearInterval(self.guidedSearchListInterval);
                        self.searchList = self._searchService.searchListObject;
                        self.refreshPage();
                    }
                }, 200);
            }
        } else {
            this.searchList = searchObject;
            this.refreshPage();
        }
    }

    refreshPage() {
        this.favoritesCount = this._userService.favoritesListObject.length;

        this.allJobList = this.searchList;
        if (this.searchList !== null) {
            this.allJobList = this.searchList.filter(job => job.show === true);
        }
        this.careersLength = this.allJobList.length;
        // Get saved selections
        this.getSelections();
        this._spinner.hide();
        this.resetPagination() ;
    }

    setGridView() {

        this.detailView = false;
        this.gridView = true;
        this.pageIndex = 0;
        this.pageSize = 6;
        this.lowValue = 0;
        this.highValue = 6;
    }


    setDetailView() {

        this.detailView = true;
        this.gridView = false;
        this.pageIndex = 0;
        this.pageSize = 1;
        this.lowValue = 0;
        this.highValue = 1;
    }


    /**
     * Pagination
     */
    getPaginatorData( event ) {
        if ( event.pageIndex === this.pageIndex + 1 ) {
            this.lowValue = this.lowValue + this.pageSize;
            this.highValue = this.highValue + this.pageSize;
        } else {
          if ( event.pageIndex === this.pageIndex - 1 ) {
            this.lowValue = this.lowValue - this.pageSize;
            this.highValue = this.highValue - this.pageSize;
          }
        }
        this.pageIndex = event.pageIndex;
    }

    // Next page for career tiles
    nextPage() {
        this.vc.nextPage();
    }

    // Increment page number for quiz.
    qNextPage() {
      this.qId++;
      this.saveSelections();
    }

    // Prev page for career tiles
    previousPage() {
        this.vc.previousPage();
    }


    firstPage() {
        if (this.vc) {
          this.vc.firstPage();
        }
    }


    safeURL( mcId: string ): SafeResourceUrl {
        const imageUrl = this._config.getImageUrl();

        return this.sanitizer.bypassSecurityTrustStyle( 'url(' + imageUrl + 'citm-images/' + mcId + '.jpg)' );
    }



    filterGuidedExplorationList( list ): any[] {

        let filteredList;

        // filter interest
        if ( !this.filterItems.R && !this.filterItems.I && !this.filterItems.A && !this.filterItems.S && !this.filterItems.E && !this.filterItems.C ) {
            filteredList = list;
        } else {
            filteredList = list.filter( item => this.filterItems[item.interestOne] || this.filterItems[item.interestTwo] || this.filterItems[item.interestThree] );
        }

        // filter work environment
        if ( !this.workEnvironmentItems['1'] && !this.workEnvironmentItems['2'] && !this.workEnvironmentItems['3'] && !this.workEnvironmentItems['4'] && !this.workEnvironmentItems['5'] && !this.workEnvironmentItems['6'] && !this.workEnvironmentItems['7'] && !this.workEnvironmentItems['8'] && !this.workEnvironmentItems['9'] && !this.workEnvironmentItems['10'] && !this.workEnvironmentItems['11'] && !this.workEnvironmentItems['12'] && !this.workEnvironmentItems['13'] && !this.workEnvironmentItems['14'] ) {
            filteredList = filteredList;
        } else {
            filteredList = filteredList.filter( item => this.workEnvironmentItems[item.codeOne] || this.workEnvironmentItems[item.codeTwo] || this.workEnvironmentItems[item.codeThree] || this.workEnvironmentItems[item.codeFour] );
        }

        // filter skill
        if ( !this.skillCheckBox.M && !this.skillCheckBox.V && !this.skillCheckBox.S ) {
            filteredList = filteredList;
        } else {
            filteredList = filteredList.filter( item => ( this.skillCheckBox.M && item.math >= 4 ) || ( this.skillCheckBox.V && item.verbal >= 4 ) || ( this.skillCheckBox.S && item.science >= 4 ) );
        }

        this.careersLength = filteredList.length;
        return filteredList;
    }


    resetPagination() {
        this.firstPage();
        this.lowValue = 0;

        if ( this.gridView ) {
            this.highValue = 6;
        } else if ( this.detailView ) {
            this.highValue = 1;
        }
        this.saveSelections();
    }


    clearAnswers() {
        this.currentPage = 1;
        this.qId = 1;
        this.workEnvironmentItems = {
            1: false,
            2: false,
            3: false,
            4: false,
            5: false,
            6: false,
            7: false,
            8: false,
            9: false,
            10: false,
            11: false,
            12: false,
            13: false,
            14: false
        };
        this.filterItems = {
            'R': false,
            'I': false,
            'A': false,
            'S': false,
            'E': false,
            'C': false
        };
        this.skillCheckBox = {
            'M': false,
            'S': false,
            'V': false
        };
        this.guidedExplorationCurrentIndex = 0;
        this.isGuidedExplorationViewResults = false;
        this.resetPagination();
    }

    /**
     * Redirect to details page.
     * @param mcId
     */
    goToDetailsPage( mcId ) {
        this._router.navigate( ['career-detail/' + mcId] );
    }

    changePage(index) {
       this.qId = index;
       this.saveSelections();
    }

  // Saves selections on input change.
  saveSelections() {
    this.guidedSelection = {
      EnvironmentItems: this.workEnvironmentItems,
      filterItems: this.filterItems,
      skillCheckBox: this.skillCheckBox,
      qId: this.qId,
      viewResults: this.isGuidedExplorationViewResults,
      currentJob: this.guidedExplorationCurrentIndex
    };
    sessionStorage.setItem('guidedSelections', JSON.stringify(this.guidedSelection));
  }

  // get selections from storage
  getSelections() {
    if (sessionStorage.getItem('guidedSelections') === null ||
        sessionStorage.getItem('guidedSelections') === 'undefined') {
      // Create new selections if not stored yet
      this.saveSelections();
    } else {
      // Load stored selections
      this.guidedSelection = JSON.parse(sessionStorage.getItem('guidedSelections'));
      console.debug('guided saved items:', this.guidedSelection);
      this.workEnvironmentItems = this.guidedSelection.EnvironmentItems;
      this.filterItems = this.guidedSelection.filterItems;
      this.skillCheckBox = this.guidedSelection.skillCheckBox;
      this.qId = this.guidedSelection.qId;
      this.isGuidedExplorationViewResults = this.guidedSelection.viewResults;
      this.guidedExplorationCurrentIndex = this.guidedSelection.currentJob;
      if (this.qId === 0) { this.qId = 1; }
    }
  }

  onFavoriteClick(item) {
        if (this.disabled) {
          return;
        }
        this.disabled = true;
        this.myStyle =  {'cursor': 'wait'};
        const favorite = item.favorited;
        return this._favoriteService.onFavoriteClick(item)
        .then (() => {
            this.favoritesCount = this._userService.favoritesListObject.length;
            this.disabled = false;
            this.myStyle =  {'cursor': 'pointer'};
            return;
        })
        .catch(() => {
          this.disabled = false;
          this.myStyle =  {'cursor': 'pointer'};
          return;
        });
    }

  isLoggedIn() {
    return this._config.isLoggedIn();
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
        this.navigationSubscription.unsubscribe();
    }
  }
}
