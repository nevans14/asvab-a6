import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router, NavigationEnd} from '@angular/router';
import { MatDialog } from '@angular/material';
import { LoginDialogComponent } from 'app/core/dialogs/login-dialog/login-dialog.component';
import { SessionService } from '../services/session.service';
import { ConfigService } from 'app/services/config.service';
import { ContentManagementService } from 'app/services/content-management.service';
import { UtilityService } from '../services/utility.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
  showSlideMenu = false;
  displayBanner: boolean;
  bannerText: String;
  disableLogin = false;
  page: any;

  constructor(
    private _dialog: MatDialog,
    private _config: ConfigService,
    private _router: Router,
    private _cmsService: ContentManagementService,
    private _sessionService: SessionService,
    private _utility: UtilityService,
  ) { }

  ngOnInit() {
    this.disableLogin = this._config.getDisabledLogin();
    this.displayBanner = this._config.getDisplayBanner();
    if (this.displayBanner) {
      this._cmsService.getPageByPageName('banner').subscribe(
        data => {
          this.page = data;
          this.bannerText = this.page.pageHtml;
      });
    }

    this._router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this._utility.removeCanonicalUrl();
        this._utility.createCanonicalUrl('https://www.careersinthemilitary.com' + this._router.url);
      }
    })
  }

  onOpenSlideOut() {
    this.showSlideMenu = true;
  }

  onLogOff() {
    this._sessionService.logOff();
    this.onClose();
  }

  onRouteChange(path) {
    this._router.navigate([path]);
    this.onClose();
  }

  isLoggedIn() {
    return this._config.isLoggedIn();
  }

  onClose() {
    this.showSlideMenu = false;
  }

  notHomePage() {
    const currentPath = this._router.url;
      if (currentPath.indexOf('/home') < 0) {
        return true;
      }
      return false;
  }


  openLoginDialog() {
    const dialogRef = this._dialog.open( LoginDialogComponent, { hasBackdrop: true, disableClose: true, autoFocus: true } );
    this.onClose();

    const self = this;
    dialogRef.afterClosed().subscribe( results => {
      if (this._config.isLoggedIn() && !this._config.getSSO()) {
        self.onRouteChange('/profile');
      }
    });

  }
}
