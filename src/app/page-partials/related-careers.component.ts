import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CareerDetailService } from 'app/services/career-detail.service';
import { RecentlyViewedService } from 'app/services/recently-viewed.service';
import { ConfigService } from 'app/services/config.service';
import { SearchListService } from 'app/services/search-list.service';
import { Router } from '@angular/router';
import { GoogleAnalyticsService } from '../services/google-analytics.service';

@Component({
  selector: 'app-related-careers',
  templateUrl: './related-careers.component.html',
  styleUrls: ['./related-careers.component.scss']
})
export class RelatedCareersComponent implements OnInit {
  readonly keyName = 'recentlyViewed';
  showMore = true;
  showMore2 = true;
  showMore3 = true;
  showMore4 = true;
  showMore5 = true;
  relatedCareer;
  relatedCivilianCareer;
  recentlyViewed: any;
  additionalInfo;
  additionalAdded = false;
  displayList: any;
  militaryCareer;
  hotJobList;
  searchList: any;
  searchListInterval: any;
  showSpinner = false;
  currentMcId;

  @Input()
  mcId: string;
  @Output()
  relatedCareerChange = new EventEmitter();

  profilePage = false;

  constructor(
    private _router: Router,
    private _searchService: SearchListService,
    private _careerDetailService: CareerDetailService,
    private _recentlyViewedService: RecentlyViewedService,
    private _config: ConfigService,
    private _googleAnalyticsService: GoogleAnalyticsService
  ) { }

  ngOnInit() {
    const url = this._router.url;
    this.profilePage = (url.indexOf('career-profile') > -1);
    this.currentMcId = this.mcId;
    this.loadData();
  }

  loadData() {
    this.getRecentlyViewed();

    this._careerDetailService.getRelatedCareer(this.currentMcId).subscribe(data => {
      this.relatedCareer = data;
    });

    this._careerDetailService.getRelatedCivilianCareer(this.currentMcId).subscribe(data => {
      this.relatedCivilianCareer = data;
      for (let i = 0; i < this.relatedCivilianCareer.length; i++) {
        this.relatedCivilianCareer[i].link = this._config.getSsoUrl() + '/occufind-occupation-details/' +
          this.relatedCivilianCareer[i].relatedId;
      }
    });

    if (!this.profilePage) {
      const self = this;
      this._careerDetailService.getAdditionalInfo(this.currentMcId).subscribe(data => {
        self.additionalInfo = data;
        // Add options that appear in live as hard coded - file has no entries that have a mc_id value
        self.addAdditionalInfo();
      });
    }

    if (this.profilePage) {
      this._careerDetailService.getHotJobTitles(this.currentMcId).subscribe(data => {
        this.hotJobList = data;
        this.refreshData();
      });
    }
  }

  ga(param) {
    this._googleAnalyticsService.trackClick(param);
  }

  gaSocialShare(socialPlug, value) {
    this._googleAnalyticsService.trackSocialShare(socialPlug, value);
  }

  addAdditionalInfo() {
      // Add options that appear in live as hard coded - file has no entries that have a mc_id value
      this.additionalInfo.push({
        title: 'Enlistment Requirements',
        link: '/options-enlistment-requirements'
      });
      this.additionalInfo.push({
        title: 'Physical Requirements',
        link: '/options-boot-camp-services'
      });
      this.additionalInfo.push({
        title: 'Find a Recruiter',
        link: '/options-contact-recruiter'
      });
      this.additionalAdded = true;
  }

  refreshData() {
    if (this.hotJobList) {
      for (let x = 0; x < this.hotJobList.length; x++) {
        this.hotJobList[x].title = '<strong>U.S. ' + this.getBranch(this.hotJobList[x].svc) + '</strong> ' +
         (this.hotJobList[x].mocTitle ? this.hotJobList[x].mocTitle : '');
      }
    }
  }

  getRecentlyViewed() {
    this.showSpinner = true;
    const searchObject = this._searchService.getSearchList();
    const self = this;
    if (!searchObject) {
      if (!this.searchListInterval) {
        this.searchListInterval = setInterval(function () {
          if (!self._searchService.getProcessingSearchList()) {
            clearInterval(self.searchListInterval);
            self.searchList = self._searchService.searchListObject;
            self.showSpinner = false;
            self.processRecentlyViewed();
          }
        }, 200);
      }
    } else {
      this.searchList = this._searchService.searchListObject;
      this.showSpinner = false;
    }

    return this._recentlyViewedService.getRecentlyViewed()
      .then((obj) => {
        this.recentlyViewed = obj;
        // check if got from local storage or no records exist
        if ((this.recentlyViewed.length > 0 && this.recentlyViewed[0].title) || this.recentlyViewed.length === 0 ) {
          this.showSpinner = false;
        }
        this.processRecentlyViewed();
        return;
      });

  }

  getTitle() {
    let title;
    for (let i = 0; i < this.searchList.length; i++) {
      if (this.currentMcId === this.searchList[i].mcId) {
        title = this.searchList[i].title;
        return title;
      }
    }
  }

  processRecentlyViewed() {
    this.displayList = [];
    if (this.searchList && this.recentlyViewed) {
      for (let x = 0; x < this.recentlyViewed.length; x++) {
        for (let y = 0; y < this.searchList.length; y++) {
          if (this.recentlyViewed[x].mcId === this.searchList[y].mcId) {
            this.recentlyViewed[x].title = this.searchList[y].title;
            break;
          }
        }
        // set this display list without value of one displayed
        if (this.currentMcId !== this.recentlyViewed[x].mcId) {
          this.displayList.push( this.recentlyViewed[x] );
        }
      }
      return this._recentlyViewedService.setRecentlyViewed(this.currentMcId, this.getTitle())
      .then ((obj) => {
       return;
      });
    }
  }

  getBranch(srv) {
    switch (srv) {
      case ('M'):
        return 'Marines';
      case ('F'):
        return 'Air Force';
      case ('N'):
        return 'Navy';
      case ('A'):
        return 'Army';
      case ('C'):
        return 'Coast Guard';
    }
  }

  onClick(mcId) {
    this.profilePage = false;
    this.currentMcId = mcId;
    this._router.navigate( ['career-detail/' + mcId] );
    this.loadData();
    window.scroll(0, 0);
  }

  openNewTab(url) {
    window.open(url, '_blank');
}

  public onRelatedCareerChange() {
    this.relatedCareerChange.emit(this.relatedCareer);
  }
}
