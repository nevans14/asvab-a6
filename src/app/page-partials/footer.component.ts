import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ConfigService } from '../services/config.service';
import { PrivacySecurityDialogComponent } from '../core/dialogs/privacy-security-dialog/privacy-security-dialog.component';

@Component( {
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
} )
export class FooterComponent implements OnInit {
    versionText = '';

    constructor(
        private dialog: MatDialog,
        private _config: ConfigService
        ) { }

    ngOnInit() {
        this.versionText = 'v ' + this._config.getVersion() + '  Last Updated:  ' + this._config.getVersionDate();
    }

    openDialog() {
        this.dialog.open( PrivacySecurityDialogComponent, {hasBackdrop: true, disableClose: true, autoFocus: true} );
    }
}
