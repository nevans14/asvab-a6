import { AfterViewChecked, ChangeDetectorRef, Component, OnInit, Optional } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DomSanitizer, Meta, Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { FindRecruiterDialogComponent } from 'app/core/dialogs/find-recruiter-dialog/find-recruiter-dialog.component';
import { NotesDialogComponent } from 'app/core/dialogs/notes-dialog/notes-dialog.component';
import { UpdateUserInfoComponent } from 'app/core/dialogs/update-user-info/update-user-info.component';
import { GuidedExplorationComponent } from 'app/guided-exploration/guided-exploration.component';
import { CareerSearchService } from 'app/services/career-search.service';
import { ConfigService } from 'app/services/config.service';
import { FavoritesService } from 'app/services/favorites.service';
import { PortfolioService } from 'app/services/portfolio.service';
import { SearchListService } from 'app/services/search-list.service';
import { UserService } from 'app/services/user.service';
import { throwError } from 'rxjs';
import { GoogleAnalyticsService } from '../services/google-analytics.service';

@Component( {
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
} )
export class ProfileComponent implements OnInit, AfterViewChecked {
    imageUrl: any;
    disabled = false;
    myStyle = { 'cursor': 'pointer' };
    public searchListObject: any;
    public searchListObjectLength: number;
    occupationNotes: any;
    profileSearchListInterval;
    public serviceContacts: any;
    userProfileInfo: any;
    notificationsServiceOffering: any;
    notificationCareerCluster: any;
    notificationHotJob: any;
    numberFavoriteHotJob: any;
    notificationInterest: any;
    favoriteList: any;
    careerClusterFavoriteList: any;
    serviceFavoriteList: any;
    cepFavoriteOccupationsList: any;
    resourceSpecific: any;
    resourceGeneric: any;
    careerClusterList: any;
    filteredResourceSpecific: any;
    filteredResourceGeneric: any;
    saveSearchList: any;
    numberOfTicks = 0;
    weNoticedOpen = true;

    /**
     * Guided Exploration
    */
    guidedExplorationJobList: any;
    guidedExplorationCurrentPage = 1;
    guidedExplorationCurrentIndex = 0;
    isGuidedExplorationViewResults = false;
    guidedExplorationListlength: number;
    getItem: any;
 

    guidedSelection = {
      EnvironmentItems: this.guidedExploration.workEnvironmentItems,
      filterItems: this.guidedExploration.filterItems,
      skillCheckBox: this.guidedExploration.skillCheckBox,
      qId: this.guidedExplorationCurrentPage,
      viewResults: this.isGuidedExplorationViewResults,
      currentJob: this.guidedExplorationCurrentIndex
    };


    // Hot Jobs pagination
    hotJobList: any;
    hotJobCurrentPage = 1;
    hotJobCurrentIndex = 0;
    hotJobListlength: number;
    hjItem: any;

    constructor(
        private _config: ConfigService,
        private _userService: UserService,
        private _portfolioService: PortfolioService,
        private _searchService: SearchListService,
        private _careerSearchService: CareerSearchService,
        private _favoritesService: FavoritesService,
        private router: Router,
        private guidedExploration: GuidedExplorationComponent,
        private _dialog: MatDialog,
        private _meta: Meta,
        private _titleTag: Title,
        private _sanitizer: DomSanitizer,
        private _googleAnalyticsService: GoogleAnalyticsService,
        @Optional() private _ref: ChangeDetectorRef
    ) {
        setInterval(() => {
            this.numberOfTicks++;
            // the following is required, otherwise the view will not be updated
            this._ref.markForCheck();
        }, 1000);
    }

    ngOnInit() {
        // Did it this way for speed
        if (!this._config.isLoggedIn()) {
            this.router.navigate(['home/']);
            return;
        }

        // Do this check incase this is a refresh of the page so no error
        if (this._config.isLoggedIn()) {
            this._titleTag.setTitle('Profile | Careers in the Military');
            this._meta.updateTag({name: 'description', content: 'Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests.'});
            
            this._meta.updateTag({ property: 'og:title', content: 'Profile | Careers in the Military' });
            this._meta.updateTag({ property: 'og:description', content: 'Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests.' });
            this._meta.updateTag({ property: 'og:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });
            this._meta.updateTag({ name: 'twitter:title', content: 'Profile | Careers in the Military' });
            this._meta.updateTag({ name: 'twitter:description', content: 'Careers in the Military powered by ASVAB Career Exploration Program allows students to explore different jobs across Services in the context of their skills and interests.' });
            this._meta.updateTag({ name: 'twitter:image', content: 'http://www.asvabprogram.com/media-center-content/thumbnails/social-share/CITM_Webshare.jpg' });
    
            this.getUserInfo();

            this.getFavoriteList();

            this._userService.getFavoriteServices().subscribe(
            data => {
                this.serviceFavoriteList = data ? data : [];

                this.generateNotifications();
            });

            this._userService.getFavoriteCareerCluster().subscribe(
            data => {
                this.careerClusterFavoriteList = data ? data : [];
                this.filterResourceSpecific();
            });

            this._careerSearchService.getCareerCluster().subscribe(
                data => {
                    this.careerClusterList = data ? data : [];
                    this.generateNotifications();
                });

            // Load saved Guided Exploration settings
            this.guidedExplorationGetSelections();

            const searchObject = this._searchService.getSearchList();
            const self = this;
            if (!searchObject) {
                if (!this.profileSearchListInterval) {
                    this.profileSearchListInterval = setInterval(function () {
                        if (!self._searchService.getProcessingSearchList()) {
                            clearInterval(self.profileSearchListInterval);
                            self.setSearchList();
                            self.generateNotifications();
                            self.updateGuidedExplorationList();
                        }
                    }, 200);
                }
            } else {
                this.setSearchList();
                this.generateNotifications();
                this.updateGuidedExplorationList();
            }

            // service contacts
            //this.serviceContacts = [];
            this._careerSearchService.getServiceContacts().subscribe(
              data => {
                this.serviceContacts = data;
            });
            //  this.serviceContacts = this.route.snapshot.data.serviceContacts;

            this.getNotes();

            this.getSavedSearches();

            this._portfolioService.getResourceGeneric().subscribe(data => {
                this.resourceGeneric = data;
            });

            this._portfolioService.getResourceSpecific().subscribe(data => {
                this.resourceSpecific = data;
                this.filterResourceSpecific();
            });

            this._portfolioService.getCepFavoriteOccupations().subscribe(data => {
                this.cepFavoriteOccupationsList = data;
            });
        }
    }

    ngAfterViewChecked() {
      this._ref.detectChanges();
    }

    getUserInfo() {
        const self = this;
        this._portfolioService.getUserInfo().subscribe(data => {
            if (data && data.length > 0) {
                self.userProfileInfo = data[0];
                this.imageUrl = this._config.getImageUrl() + 'citm-uploads/' + self.userProfileInfo.userId + '.png?' + Date.now();
                this._portfolioService.getFromUrl(this.imageUrl).subscribe(
                    res => res,
                    err => {
                        if (err && err.indexOf('200') <= -1) {
                            this.imageUrl = 'assets/images/profile.jpg'
                        }
                    },
                )
            } else {
                this.userProfileInfo = {
                    'name' : '',
                    'schoolName' : '',
                    'graduationYear': '',
                    'photo': null
                };
                this.imageUrl = 'assets/images/profile.jpg';
            }
        });
    }

    getFavoriteList() {
        if (this._config.isLoggedIn()) {
            this._userService.getFavoritesList().subscribe(
                data => {
                    this.favoriteList = data ? data : [];

                    for (let i = 0; i < this.favoriteList.length; i++) {
                        this.favoriteList[i].checked = false;
                    }
                    this._ref.detectChanges();
                    this.generateNotifications();
                    this.filterResourceSpecific();
                });
        }
    }


    getNotes() {
        this._userService.getNotes().subscribe(data => {
            this.occupationNotes = data;
            for (let i = 0; i < this.occupationNotes.length; i++) {
                this.occupationNotes[i].notesLimit = this.occupationNotes[i].notes.substring(0, 100);
            }
            this._ref.detectChanges();
        });
    }

    getSavedSearches() {
        this._userService.getSavedSearches().subscribe(data => {
            this.saveSearchList = data;
            this._ref.detectChanges();
        });

    }

    openNewTab(url) {
        window.open(url, '_blank');
    }

  setSearchList() {
      this.searchListObject = this._searchService.searchListObject.filter(job => job.show === true);
      this.searchListObjectLength = this.searchListObject.length;
      this.hotJobList = this.searchListObject.filter(job => job.isHotJob === 'T' && job.show === true);
      this.hotJobListlength = this.hotJobList.length;
      this.hotJobCurrentIndex = 0;
      this.hotJobCurrentPage = 1;
      this.hjItem = this.hotJobList[this.hotJobCurrentIndex];
}

    /**
    * Filter for suggested resource
    */
    filterResourceSpecific() {
        if (this.resourceSpecific && this.favoriteList && this.careerClusterFavoriteList) {
            this.filteredResourceSpecific = [];
            for (let x = 0; x < this.resourceSpecific.length; x++) {
                for (let i = 0; i < this.favoriteList.length; i++) {
                    if (this.favoriteList[i].mcId === this.resourceSpecific[x].mcId) {
                        if (this.checkNotDuplicateValue(this.resourceSpecific[x].title, 'specific')) {
                            this.filteredResourceSpecific.push(this.resourceSpecific[x]);
                        }
                    }
                }
            }

            for (let x = 0; x < this.resourceSpecific.length; x++) {
                for (let i = 0; i < this.careerClusterFavoriteList.length; i++) {
                    if (this.careerClusterFavoriteList[i].ccId === this.resourceSpecific[x].ccId) {
                        if (this.checkNotDuplicateValue(this.resourceSpecific[x].title, 'specific')) {
                            this.filteredResourceSpecific.push(this.resourceSpecific[x]);
                        }
                    }
                }
            }
            this.filterResourceGeneric();
        }
    }

    /**
    * Filter for suggested resource
    */
    filterResourceGeneric() {
        if (this.resourceGeneric && this.favoriteList && this.careerClusterList) {
            this.filteredResourceGeneric = [];
            for (let x = 0; x < this.resourceGeneric.length; x++) {
                if (this.checkNotDuplicateValue(this.resourceGeneric[x].title, 'generic')) {
                    this.filteredResourceGeneric.push(this.resourceGeneric[x]);
                }
            }
        }
        // get distinct values
    }

    checkNotDuplicateValue(value, type) {
        if (type === 'specific') {
            for (let i = 0; i < this.filteredResourceSpecific.length; i++) {
                if (this.filteredResourceSpecific[i].title === value) {
                    return false;
                }
            }
            return true;
        }

        if (type === 'generic') {
            for (let i = 0; i < this.filteredResourceGeneric.length; i++) {
                if (this.filteredResourceGeneric[i].title === value) {
                    return false;
                }
            }
            return true;
        }
    }

  checkDisabled() {
    return this.disabled;
  }

  openFindRecruiterModal(){
    const dialogRef = this._dialog.open( FindRecruiterDialogComponent, {
      data: { serviceContacts: this.serviceContacts }
    });
  }

  openUserInfoDialog() {
    const data = {
        file: this.imageUrl,
        data: this.userProfileInfo
    };
    const dialogRef = this._dialog.open( UpdateUserInfoComponent, {
        hasBackdrop: true,
        disableClose: true,
        autoFocus: true,
        data: data
    });
    const self = this;
    dialogRef.afterClosed().subscribe(result => {
      console.log('Update User Info dialog was closed');
        self.getUserInfo();
    });
  }

  viewSaveSearch(search) {
    this._searchService.currentSearchCriteria = search;
    this.router.navigate( ['/advanced-search'] );
  }

    // guidedExplorationGetPaginatorData( event ) {
    //     if ( event.pageIndex === this.guidedExplorationPageIndex + 1 ) {
    //         this.guidedExplorationLowValue = this.guidedExplorationLowValue + this.guidedExplorationPageSize;
    //         this.guidedExplorationHighValue = this.guidedExplorationHighValue + this.guidedExplorationPageSize;
    //     } else if ( event.pageIndex === this.guidedExplorationPageIndex - 1 ) {
    //         this.guidedExplorationLowValue = this.guidedExplorationLowValue - this.guidedExplorationPageSize;
    //         this.guidedExplorationHighValue = this.guidedExplorationHighValue - this.guidedExplorationPageSize;
    //     }
    //     this.guidedExplorationPageIndex = event.pageIndex;
    // }

    guidedExplorationNextPage() {
        if (this.guidedExplorationCurrentIndex + 1 < this.guidedExplorationJobList.length) {
            this.guidedExplorationCurrentIndex++;
            this.getItem = this.guidedExplorationJobList[this.guidedExplorationCurrentIndex];
            this.guidedExplorationSaveSelections();
        }
    }

    guidedExplorationPreviousPage() {
        if (this.guidedExplorationCurrentIndex  > 0) {
            this.guidedExplorationCurrentIndex--;
            this.getItem = this.guidedExplorationJobList[this.guidedExplorationCurrentIndex];
            this.guidedExplorationSaveSelections();
        }
    }

    guidedExplorationChangePage(index) {
       this.guidedExplorationCurrentPage = index;
       this.guidedExplorationSaveSelections();
    }

    filterGuidedExplorationList( list ) {

        let filteredList;
        list = list.filter( job => job.show === true);

        // filter interest
        if ( !this.guidedExploration.filterItems.R && !this.guidedExploration.filterItems.I &&
            !this.guidedExploration.filterItems.A && !this.guidedExploration.filterItems.S &&
            !this.guidedExploration.filterItems.E && !this.guidedExploration.filterItems.C ) {
            filteredList = list;
        } else {
            filteredList = list.filter( item => this.guidedExploration.filterItems[item.interestOne] ||
                this.guidedExploration.filterItems[item.interestTwo] || this.guidedExploration.filterItems[item.interestThree] );
        }

        // filter work environment
        if ( !this.guidedExploration.workEnvironmentItems['1'] && !this.guidedExploration.workEnvironmentItems['2']
        && !this.guidedExploration.workEnvironmentItems['3'] && !this.guidedExploration.workEnvironmentItems['4']
        && !this.guidedExploration.workEnvironmentItems['5'] && !this.guidedExploration.workEnvironmentItems['6']
        && !this.guidedExploration.workEnvironmentItems['7'] && !this.guidedExploration.workEnvironmentItems['8']
        && !this.guidedExploration.workEnvironmentItems['9'] && !this.guidedExploration.workEnvironmentItems['10']
        && !this.guidedExploration.workEnvironmentItems['11'] && !this.guidedExploration.workEnvironmentItems['12']
        && !this.guidedExploration.workEnvironmentItems['13'] && !this.guidedExploration.workEnvironmentItems['14'] ) {
            filteredList = filteredList;
        } else {
            filteredList = filteredList.filter( item => this.guidedExploration.workEnvironmentItems[item.codeOne] ||
                this.guidedExploration.workEnvironmentItems[item.codeTwo] || this.guidedExploration.workEnvironmentItems[item.codeThree] ||
                this.guidedExploration.workEnvironmentItems[item.codeFour] );
        }

        // filter skill
        if ( !this.guidedExploration.skillCheckBox.M
            && !this.guidedExploration.skillCheckBox.V && !this.guidedExploration.skillCheckBox.S ) {
            filteredList = filteredList;
        } else {
            filteredList = filteredList.filter( item => ( this.guidedExploration.skillCheckBox.M
                && item.math >= 4 ) || ( this.guidedExploration.skillCheckBox.V && item.verbal >= 4 )
                || ( this.guidedExploration.skillCheckBox.S && item.science >= 4 ) );
        }

        this.guidedExplorationListlength = filteredList.length;
        this.guidedExplorationJobList = filteredList;
        // this.guidedExplorationCurrentIndex = 0;
        // this.guidedExplorationCurrentPage = 1;
        this.getItem = this.guidedExplorationJobList[this.guidedExplorationCurrentIndex];
    }

    guidedExplorationNextQuestion() {
        this.guidedExplorationCurrentPage++;
        this.guidedExplorationSaveSelections();
    }

    guidedExplorationViewResults() {
        this.filterGuidedExplorationList( this.searchListObject );
        this.isGuidedExplorationViewResults = true;
        this.guidedExplorationSaveSelections();
    }

    updateGuidedExplorationList() {
        if (this.isGuidedExplorationViewResults === true) {
         this.filterGuidedExplorationList( this.searchListObject );
        }
    }

    guidedExplorationRetake() {
        this.guidedExploration.workEnvironmentItems = {
            1: false,
            2: false,
            3: false,
            4: false,
            5: false,
            6: false,
            7: false,
            8: false,
            9: false,
            10: false,
            11: false,
            12: false,
            13: false,
            14: false
        };
        this.guidedExploration.filterItems = {
            'R': false,
            'I': false,
            'A': false,
            'S': false,
            'E': false,
            'C': false
        };
        this.guidedExploration.skillCheckBox = {
            'M': false,
            'S': false,
            'V': false
        };

        this.guidedExplorationCurrentIndex = 0;
        this.guidedExplorationCurrentPage = 1;
        this.isGuidedExplorationViewResults = false;

        this.guidedExplorationSaveSelections();
    }

    // Saves selections on input change.
    guidedExplorationSaveSelections() {
        this.guidedSelection = {
            EnvironmentItems: this.guidedExploration.workEnvironmentItems,
            filterItems: this.guidedExploration.filterItems,
            skillCheckBox: this.guidedExploration.skillCheckBox,
            qId: this.guidedExplorationCurrentPage,
            viewResults: this.isGuidedExplorationViewResults,
            currentJob: this.guidedExplorationCurrentIndex
        };
        sessionStorage.setItem('guidedSelections', JSON.stringify(this.guidedSelection));
    }

    // get selections from storage
    guidedExplorationGetSelections() {
        if (sessionStorage.getItem('guidedSelections') === null ||
            sessionStorage.getItem('guidedSelections') === 'undefined') {
            // Create new selections if not stored yet
            this.guidedExplorationSaveSelections();
        } else {
            // Load stored selections
            this.guidedSelection = JSON.parse(sessionStorage.getItem('guidedSelections'));
            // console.debug('guided saved items:', this.guidedSelection);
            this.guidedExploration.workEnvironmentItems = this.guidedSelection.EnvironmentItems;
            this.guidedExploration.filterItems = this.guidedSelection.filterItems;
            this.guidedExploration.skillCheckBox = this.guidedSelection.skillCheckBox;
            this.guidedExplorationCurrentPage = this.guidedSelection.qId;
            this.isGuidedExplorationViewResults = this.guidedSelection.viewResults;
            this.guidedExplorationCurrentIndex = this.guidedSelection.currentJob;
            if (this.guidedExplorationCurrentPage === 0) { this.guidedExplorationCurrentPage = 1; }
            // console.debug('done results:', this.isGuidedExplorationViewResults);
        }
    }

    // hotJobGetPaginatorData( event ) {
    //     if ( event.pageIndex === this.hotJobPageIndex + 1 ) {
    //         this.hotJobLowValue = this.hotJobLowValue + this.hotJobPageSize;
    //         this.hotJobHighValue = this.hotJobHighValue + this.hotJobPageSize;
    //     } else if ( event.pageIndex === this.hotJobPageIndex - 1 ) {
    //         this.hotJobLowValue = this.hotJobLowValue - this.hotJobPageSize;
    //         this.hotJobHighValue = this.hotJobHighValue - this.hotJobPageSize;
    //     }
    //     this.hotJobPageIndex = event.pageIndex;
    // }

    hotJobNextPage() {
        if (this.hotJobCurrentIndex + 1 < this.hotJobList.length) {
            this.hotJobCurrentIndex++;
            this.hotJobCurrentPage = this.hotJobCurrentIndex + 1;
            this.hjItem = this.hotJobList[this.hotJobCurrentIndex];
        }
    }

    hotJobPreviousPage() {
        if (this.hotJobCurrentIndex  > 0) {
            this.hotJobCurrentIndex--;
            this.hotJobCurrentPage = this.hotJobCurrentIndex + 1;
            this.hjItem = this.hotJobList[this.hotJobCurrentIndex];
        }
    }

    deleteSaveSearch(search) {
        // Disable link if process was already started.
        if (this.disabled) {
            return false;
        } else {
            this.disabled = true;
        }

        this.myStyle = { 'cursor': 'wait' };
        const self = this;
        return this._userService.deleteSavedSearch(search.id)
            .then(function (response) {
                self.getSavedSearches();
                self.myStyle = { 'cursor': 'pointer' };
                self.disabled = false;
                return;
            })
            .catch(error => {
                self.myStyle = { 'cursor': 'pointer' };
                self.disabled = false;
                throwError(error);
            });
    }

    onFavoriteClick(item) {
        this._favoritesService.onFavoriteClick(item);
    }

    onHiddenClick(item) {
        const self = this;
        return this._favoritesService.onHiddenClick(item)
          .then((obj) => {
              for (let i = 0; i < self.searchListObject.length; i++) {
                  if (self.searchListObject[i].mcId === item.mcId) {
                      self.searchListObject[i].show = false;
                      break;
                  }
              }
            self.hotJobList = self.searchListObject.filter(job => job.isHotJob === 'T' && job.show === true);
            self.hotJobListlength = self.hotJobList.length;
            self.hjItem = self.hotJobList[self.hotJobCurrentIndex];
            self.updateGuidedExplorationList();
            return;
          });
    }

    refreshFavorites() {
        this.getFavoriteList();
    }

    onNotesClick(note) {
        // Send the type and Occupation Id
        const noteInfo = {
          typeOfNote: 'occupational',
          title: note.title,
          mcId: note.mcId,
          note: note
        };
        const dialogRef = this._dialog.open( NotesDialogComponent, {
          data: noteInfo,
        });

        dialogRef.afterClosed().subscribe(result => {
            this.getNotes();
          });
    }

    deleteNote(note) {
        // Disable link if process was already started.
        if (this.disabled) {
            return false;
        }
        this.disabled = true;

        this.myStyle = { 'cursor': 'wait' };
        const self = this;
        return this._userService.deleteNote(note.noteId)
        .then((response) => {
            self.getNotes();
            self.myStyle = { 'cursor': 'pointer' };
            self.disabled = false;
            return;
        })
        .catch((error) => {
            self.myStyle = { 'cursor': 'pointer' };
            self.disabled = false;
            throwError(error);
        });
    }
    /***********************************************************************
	 * We noticed notifications.
	 **********************************************************************/
    generateNotifications() {
        if (this.serviceFavoriteList && this.favoriteList && this.careerClusterList && this.searchListObject) {
            this.notificationsServiceOffering = [];
            const careerClusterCount = {
                1: 0,
                2: 0,
                3: 0,
                4: 0,
                5: 0,
                6: 0,
                7: 0,
                8: 0,
                9: 0,
                10: 0,
                11: 0,
                12: 0,
                13: 0,
                14: 0,
                15: 0,
                16: 0
            };
            const interestCodeCount = {
                'R': 0,
                'I': 0,
                'A': 0,
                'S': 0,
                'E': 0,
                'C': 0
            };

            // service offering notification condition
            if (this.serviceFavoriteList.length !== 0) {

                for (let i = 0; i < this.serviceFavoriteList.length; i++) {

                    const svcId = this.serviceFavoriteList[i].svcId;
                    let numberOfOccupationsOffering = 0;
                    let svcName = null;

                    for (let j = 0; j < this.favoriteList.length; j++) {
                        let occupationInfo = null;
                        // get the services offering the occupation
                        for (let k = 0; k < this.searchListObject.length; k++) {
                            if (this.favoriteList[j].mcId === this.searchListObject[k].mcId) {
                                occupationInfo = this.searchListObject[k];
                                break;
                            }
                        }

                        // track occupation offering count for each service
                        if (occupationInfo) {
                            if (svcId === 'A' && occupationInfo.isArmy === 'T') {
                                numberOfOccupationsOffering++;
                                svcName = 'Army';
                            } else if (svcId === 'M' && occupationInfo.isMarines === 'T') {
                                numberOfOccupationsOffering++;
                                svcName = 'Marines';
                            } else if (svcId === 'N' && occupationInfo.isNavy === 'T') {
                                numberOfOccupationsOffering++;
                                svcName = 'Navy';
                            } else if (svcId === 'C' && occupationInfo.isCoastGuard === 'T') {
                                numberOfOccupationsOffering++;
                                svcName = 'Coast Guard';
                            } else if (svcId === 'F' && occupationInfo.isAirForce === 'T') {
                                numberOfOccupationsOffering++;
                                svcName = 'Air Force';
                            }
                        }

                    }

                    // push organized data to array
                    if (numberOfOccupationsOffering > 1) {
                        this.notificationsServiceOffering.push({
                            svcId: svcId,
                            svcName: svcName,
                            numberOfOccupationsOffering: numberOfOccupationsOffering
                        });
                    }
                }
            }

            // career cluster notification condition
            this.notificationCareerCluster = [];
            this.notificationHotJob = [];
            this.numberFavoriteHotJob = 0;
            for (let j = 0; j < this.favoriteList.length; j++) {

                // get the services offering the occupation
                const index2 = this.searchListObject.map(function (item) {
                    return item.mcId;
                }).indexOf(this.favoriteList[j].mcId);

                // all information on occupation
                const occupationInfo = this.searchListObject[index2];

                if (occupationInfo) {
                    // hot job notification
                    if (occupationInfo.isHotJob === 'T') {
                        this.numberFavoriteHotJob++;
                    }

                    // interest code notification count
                    if (occupationInfo.interestOne === 'R' || occupationInfo.interestTwo === 'R' ||
                        occupationInfo.interestThree === 'R') {
                        interestCodeCount['R']++;
                    } else if (occupationInfo.interestOne === 'I' || occupationInfo.interestTwo === 'I' ||
                        occupationInfo.interestThree === 'I') {
                        interestCodeCount['I']++;
                    } else if (occupationInfo.interestOne === 'A' || occupationInfo.interestTwo === 'A' ||
                        occupationInfo.interestThree === 'A') {
                        interestCodeCount['A']++;
                    } else if (occupationInfo.interestOne === 'S' || occupationInfo.interestTwo === 'S' ||
                        occupationInfo.interestThree === 'S') {
                        interestCodeCount['S']++;
                    } else if (occupationInfo.interestOne === 'E' || occupationInfo.interestTwo === 'E' ||
                        occupationInfo.interestThree === 'E') {
                        interestCodeCount['E']++;
                    } else if (occupationInfo.interestOne === 'C' || occupationInfo.interestTwo === 'C' ||
                        occupationInfo.interestThree === 'C') {
                        interestCodeCount['C']++;
                    }

                    careerClusterCount[occupationInfo.ccId]++;
                }

            }

            this.notificationInterest = [];
            if (interestCodeCount['R'] > 2) {
                this.notificationInterest.push({
                    numberofOccupations: interestCodeCount['R'],
                    question: 'Do you like to work with your hands and see concrete results?',
                    name: 'Realistic'
                });
            } else if (interestCodeCount['I'] > 2) {
                this.notificationInterest.push({
                    numberofOccupations: interestCodeCount['I'],
                    question: 'Do you like to test ideas, learn new things, and solve problems?',
                    name: 'Investigative'
                });
            } else if (interestCodeCount['A'] > 2) {
                this.notificationInterest.push({
                    numberofOccupations: interestCodeCount['A'],
                    question: 'Do you like to use your imagination and create original work?',
                    name: 'Artistic'
                });
            } else if (interestCodeCount['S'] > 2) {
                this.notificationInterest.push({
                    numberofOccupations: interestCodeCount['S'],
                    question: 'Do you like to help others and build relationships?',
                    name: 'Social'
                });
            } else if (interestCodeCount['E'] > 2) {
                this.notificationInterest.push({
                    numberofOccupations: interestCodeCount['E'],
                    question: 'Do you like to lead others and make decisions?',
                    name: 'Enterprising'
                });
            } else if (interestCodeCount['C'] > 2) {
                this.notificationInterest.push({
                    numberofOccupations: interestCodeCount['C'],
                    question: 'Do you like to organize items and work with details?',
                    name: 'Conventional'
                });
            }

            if (this.numberFavoriteHotJob > 0) {
                this.notificationHotJob.push({
                    numberFavoriteHotJob: this.numberFavoriteHotJob
                });
            }

            for (let i = 1; i <= this.careerClusterList.length; i++) {

                // get the services offering the occupation
                const index3 = this.careerClusterList.map(function (item) {
                    return item.id;
                }).indexOf(i);

                const ccInfo = this.careerClusterList[index3];

                // if more than two favorites share the same career cluster push
                // to
                // array
                if (careerClusterCount[i] > 2) {
                    this.notificationCareerCluster.push({
                        numberofOccupations: careerClusterCount[i],
                        title: ccInfo.title
                    });
                }
            }
        }
    }

    ga(param) {
        this._googleAnalyticsService.trackClick(param);
    }

    gaSocialShare(socialPlug, value) {
        this._googleAnalyticsService.trackSocialShare(socialPlug, value);
    }
}
