import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { SaveSearchComponent } from 'app/core/dialogs/save-search/save-search.component';

@Injectable({
  providedIn: 'root'
})
export class AdvancedSearchService {
  currentSearchSelections: any;

  constructor(
    private _dialog: MatDialog
  ) { }

  verifySaveSearch(valuesList, saveSearchList) {
    if (saveSearchList.length === 5) {
      const data = {
        'title': 'Alert',
        'message': 'You have reached the saved search limit.'
      };
      const dialogRef = this._dialog.open(MessageDialogComponent, {
        data: data,
        hasBackdrop: true,
        disableClose: true
      });
      return false;
    }

    if (valuesList.length < 1) {
      const data = {
        'title': 'Alert',
        'message': 'There are no selections to save. Make selections and try again.'
      };
      const dialogRef = this._dialog.open(MessageDialogComponent, {
        data: data,
      });
      return false;
    }
    return true;
  }

  saveSearch(searchValues) {

    const dialogRef2 = this._dialog.open(SaveSearchComponent, {
      data: { searchValues: searchValues, success: false }
    });

    let isSuccess = false;
    dialogRef2.afterClosed().subscribe(results => {
      isSuccess = results;
    });

    return isSuccess;
  }

  filterList(searchCriteria, searchListObject, skill, tabName) {
    const filteredList = [];
    const filteredInterests = this.getFilteredInterest(searchCriteria);

    for (let i = 0; i < searchListObject.length; i++) {
      let addItem = false;
      const item = searchListObject[i];
      const searchWord = searchCriteria.keyword ? searchCriteria['keyword'] : '';
      addItem = this.checkKeyword(item, searchWord);
      if (!addItem) {
        console.debug('Failed Keyword for ' + item.title);
        continue;
      }
     addItem = this.checkServices(searchCriteria, item);
      if (!addItem) {
        console.debug('Failed Service for ' + item.title);
        continue;
      }
      addItem = this.checkType(searchCriteria, item);
      if (!addItem) {
        console.debug('Failed Type for ' + item.title);
        continue;
      }
      addItem = this.checkInterests(searchCriteria, item, filteredInterests)
      if (!addItem) {
        console.debug('Failed Interests for ' + item.title);
        continue;
      }
      addItem = this.checkFeaturedCategory(searchCriteria, item);
      if (!addItem) {
        console.debug('Failed FC.');
        continue;
      }

      addItem = this.checkSkillImportance(skill, tabName, item);
      if (addItem) {
        filteredList.push(item);
      }
    }
    return filteredList;
  }

  checkKeyword(item, searchword) {
    if (searchword) {
      searchword = searchword.toLowerCase();
    if (!searchword ||
      item.title.toLowerCase().indexOf(searchword) !== -1 ||
      (item.mocTitle && item.mocTitle.toLowerCase().indexOf(searchword) !== -1) ||
      (this.checkOtherTitles(item.mocs, searchword)) ||
      (this.checkOtherTitles(item.alsoCalled, searchword))) {
      return true;
    }
  } else {
    return true;
  }
    return false;
  }

  checkOtherTitles(titles, searchword) {
    if (titles) {
      if (!(titles instanceof Array)) {
        titles = titles.split(';');
      }
      for (let i = 0; i < titles.length; i++) {
        if ((titles[i].mocTitle && (titles[i].mocTitle.toLowerCase()).indexOf(searchword) !== -1) ||
        (titles[i].alsoCalled && (titles[i].alsoCalled.toLowerCase()).indexOf(searchword) !== -1) ||
        (titles[i] instanceof String && (titles[i].toLowerCase()).indexOf(searchword) !== -1)) {
          return true;
        }
      }
    }
    return false;
  }

  checkServices(searchCriteria, item) {
    if ((!searchCriteria.army && !searchCriteria.marines && !searchCriteria.airForce
      && !searchCriteria.navy && !searchCriteria.coastGuard) ||
      (searchCriteria.army && item.isArmy === 'T') ||
      (searchCriteria.marines && item.isMarines === 'T') ||
      (searchCriteria.airForce && item.isAirForce === 'T') ||
      (searchCriteria.navy && item.isNavy === 'T') ||
      (searchCriteria.coastGuard && item.isCoastGuard === 'T')) {
      return true;
    }
    return false;
  }

  checkType(searchCriteria, item) {
    if ((!searchCriteria.enlisted && !searchCriteria.officer) ||
      (searchCriteria.enlisted && item.isEnlisted === 'T') ||
      (searchCriteria.officer && item.isOfficer === 'T')) {
      return true;
    } else {
      return false;
    }
  }

  checkInterests(searchCriteria, item, filteredInterests) {
    // console.debug('filteredInterests: ' + JSON.stringify(filteredInterests))
    // console.debug(`Title ${item.title}  1: ${item.interestOne} 2: ${item.interestTwo} 3: ${item.interestThree}`)

    let itemInterests = [];
    if (item.interestOne !== null) {
      itemInterests.push(item.interestOne);
    }
    if (item.interestTwo !== null) {
      itemInterests.push(item.interestTwo);
    }
    if (item.interestThree !== null) {
      itemInterests.push(item.interestThree);
    }

    let containsAll = true;
    filteredInterests.some(interest => {
      if (itemInterests.indexOf(interest) <= -1) {
        containsAll = containsAll && false;
        return !containsAll;
      }
      return !containsAll;
    })

    if (!filteredInterests || filteredInterests.length === 0 || containsAll) {
      return true;
    }
    return false;
  }

  checkCareerCluster(searchCriteria, item, filteredCC) {
    if ((!filteredCC || filteredCC.length === 0) || filteredCC.indexOf(item.ccId) > -1) {
      // Remove these code checking in the work_environment table
    // (filteredCC.indexOf(item.ccId) > -1 || filteredCC.indexOf(item.codeOne) > -1 ||
    // filteredCC.indexOf(item.codeTwo) > -1 || filteredCC.indexOf(item.codeThree) > -1 ||
    // filteredCC.indexOf(item.codeFour) > -1 )) {
   return true;
 }
 return false;

 // Left this in in case they want to go to searching for occupations with all Career clusters 
    // if (!filteredCC || filteredCC.length === 0) {
    //   return true;
    // } else {
    //   for (let i = 0; i < filteredCC.length; i++) {
    //     if ()
        // if (filteredCC[i] !== item.ccId && filteredCC[i] !== item.codeOne &&
        // filteredCC[i] !== item.codeTwo && filteredCC[i] !== item.codeThree &&
        // filteredCC[i] !== item.codeFour) {
    //       return false;
    //     }
    //   }
    // }
    // return true;
  }

  checkFeaturedCategory(searchCriteria, item) {
    if (!searchCriteria.hotJob || (searchCriteria.hotJob &&  item.isHotJob === 'T'))  {
      return true;
    }
    return false;
  }

  checkSkillImportance(skill, tabName, item) {
    switch (tabName) {
      case ('Most'):
        if (skill === 'math' && (item.math >= 4 && item.math <= 5)) {
          return true;
        }
        if (skill === 'verbal' && (item.verbal >= 4 && item.verbal <= 5)) {
          return true;
        }
        if (skill === 'science' && (item.science >= 4 && item.science <= 5)) {
          return true;
        }
        return false;

      case ('Moderate'):
        if (skill === 'math' && (item.math >= 2.5 && item.math <= 3.5)) {
          return true;
        }
        if (skill === 'verbal' && (item.verbal >= 2.5 && item.verbal <= 3.5)) {
          return true;
        }
        if (skill === 'science' && (item.science >= 2.5 && item.science <= 3.5)) {
          return true;
        }
        return false;

      case ('Least'):
        if (skill === 'math' && (item.math >= 0 && item.math <= 2)) {
          return true;
        }
        if (skill === 'verbal' && (item.verbal >= 0 && item.verbal <= 2)) {
          return true;
        }
        if (skill === 'science' && (item.science >= 0 && item.science <= 2)) {
          return true;
        }
        return false;
      default:
        return true;
    }
  }

  getFilteredInterest(searchCriteria) {
    const filteredInterests = [];

    if (searchCriteria.realistic) {
      filteredInterests.push('R');
    }
    if (searchCriteria.investigative) {
      filteredInterests.push('I');
    }
    if (searchCriteria.artistic) {
      filteredInterests.push('A');
    }
    if (searchCriteria.social) {
      filteredInterests.push('S');
    }
    if (searchCriteria.enterprising) {
      filteredInterests.push('E');
    }
    if (searchCriteria.conventional) {
      filteredInterests.push('C');
    }
    return filteredInterests;
  }
 
  getFilteredCC(searchCriteria) {
    const filteredCC = [];

    if (searchCriteria.ccOne) {
      filteredCC.push(1);
    }
    if (searchCriteria.ccTwo) {
      filteredCC.push(2);
    }
    if (searchCriteria.ccThree) {
      filteredCC.push(3);
    }
    if (searchCriteria.ccFour) {
      filteredCC.push(4);
    }
    if (searchCriteria.ccFive) {
      filteredCC.push(5);
    }
    if (searchCriteria.ccSix) {
      filteredCC.push(6);
    }
    if (searchCriteria.ccSeven) {
      filteredCC.push(7);
    }
    if (searchCriteria.ccEight) {
      filteredCC.push(8);
    }
    if (searchCriteria.ccNine) {
      filteredCC.push(9);
    }
    if (searchCriteria.ccTen) {
      filteredCC.push(10);
    }
    if (searchCriteria.ccEleven) {
      filteredCC.push(11);
    }
    if (searchCriteria.ccTwelve) {
      filteredCC.push(12);
    }
    if (searchCriteria.ccThirteen) {
      filteredCC.push(13);
    }
    if (searchCriteria.ccFourteen) {
      filteredCC.push(14);
    }
    if (searchCriteria.ccFifteen) {
      filteredCC.push(15);
    }
    if (searchCriteria.ccSixteen) {
      filteredCC.push(16);
    }
    return filteredCC;
  }

}