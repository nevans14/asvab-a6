import { Injectable } from '@angular/core';
import { HttpHelperService } from './http-helper.service';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from 'app/services/config.service';
import { Observable } from 'rxjs';
import { resolve } from 'q';

@Injectable({
  providedIn: 'root'
})

export class ScoreService {
   public scoreObject: any;
   public favorateObject: any;
    constructor(
    private _http: HttpClient,
    private _config: ConfigService,
    private _httpHelper: HttpHelperService
  ) { }

  /**
 * Get Favorites
*/
    getScore(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('testScore/get-test-score/', null));
    }

    insertScore(scoreObject) {
        return this._httpHelper.httpHelper('post', 'testScore/insertManualTestScore', null, scoreObject)
        .then((obj) => {
            resolve(this.scoreObject);
        });
    }

    updateScore(obj) {
        if (this._config.isLoggedIn()) {
            return this._httpHelper.httpHelper('put', 'testScore/updateManualTestScore', null, obj)
                .then((cfgObj) => {
                resolve(cfgObj);
            });
        }
    }

    deleteScore() {
        return this._httpHelper.httpHelper('Delete', 'testScore/deleteManualTestScore', null, null)
        .then((obj) => {
            resolve(obj);
        });
    }

    setScoreObject(score) {
        this.scoreObject = score;
    }

    getCompositeFavoritesFlags(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('testScore/getCompositeFavoritesFlags/', null));
    }

    setCompositeFavoritesFlags( favorateObject ) {
        return new Promise((resolve, reject) => {
            this._httpHelper.httpHelper('post', 'testScore/setCompositeFavoritesFlags', null, favorateObject)
                .then((obj) => {
                    resolve(obj);
                }).catch(error => {
                    reject(error);
                });
        });
    }

    deleteCompositeFavoritesFlags() {
        return this._httpHelper.httpHelper('Delete', 'testScore/deleteCompositeFavoritesFlags', null, null)
        .then((obj) => {
            resolve(obj);
        });
    }

    getServiceOfferingCompositeScore(): Observable<any> {
        return this._http.get(this._httpHelper.getFullUrl('detail-page/get-service-offering-composite/', null));
    }

    getServiceOffering(mcId): Observable<any> {
        const params = this.createMcIdParam(mcId);
        return this._http.get(this._httpHelper.getFullUrl('detail-page/get-service-offering/', params));
    }

    createMcIdParam(mcId) {
        const params = [];
        params.push({
        key: null,
        value: mcId
        });
        return params;
    }
}
