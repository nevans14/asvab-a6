import { Injectable, Inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CookieService } from 'ngx-cookie-service';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';
import { GoogleAnalyticsService } from '../services/google-analytics.service';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})

export class UtilityService {
  facebookUrl = 'https://www.facebook.com/sharer/sharer.php?';
  twitterUrl = 'https://twitter.com/intent/tweet?';
  linkedInUrl = 'https://www.linkedin.com/sharing/share-offsite/?';

  constructor(
    private _ga: GoogleAnalyticsService,
    private _cookie: CookieService,
    private _dialog: MatDialog,
    @Inject(DOCUMENT) private dom,
  ) { }

  createCanonicalUrl(url?: string) {
    let canonicalURL = url == undefined ? this.dom.URL : url;
    let link: HTMLLinkElement = this.dom.createElement('link');
    link.setAttribute('rel', 'canonical');
    link.setAttribute('id', 'canonical');
    this.dom.head.appendChild(link);
    link.setAttribute('href', canonicalURL);
  }

  removeCanonicalUrl() {
    let link = this.dom.getElementById("canonical");
    if (link) {
      link.remove();
    }
  }

  messageDialog(title, message) {
    const data = {
      title,
      message,
    };

    const dialogRef = this._dialog.open(MessageDialogComponent, {
      data: data
    });
  }

  urlEncode(url, params) {
    const encodedParams = encodeURI(params);
    return url + encodedParams;
  }

  urlDecode(data) {
    const decodedParams = decodeURI(data);
    return decodedParams;
  }

  /* 
  *    Social Sharing Functions
  */

  displaySocialShare(app, params, additionalInfo) {
    this.trackSocialShare(app, additionalInfo);
    const url = this.encodeSocialShareUrl(app, params);
    if (url) {
      window.open(url, 'popup', 'width=600,height=600');
    }
    return false;
  }

  encodeSocialShareUrl(app, params) {
    let url;
    switch (app) {
      case 'facebook':
        url = this.facebookUrl;
        break;
      case 'twitter':
        url = this.twitterUrl;
        break;
      case 'linkedin':
        url = this.linkedInUrl;
        break;
      default:
        this.messageDialog('Social Share Error', 'Social Share for ' + app + ' is not supported.');
        return;
    }
    const specializedParams = this.parmsByAppCreation(app, params);
    return this.urlEncode(url, specializedParams);
  }

  parmsByAppCreation(app, params) {
    let appParams: String;
    switch (app) {
      case 'facebook':
        appParams = 'u=' + params.url;
        break;
      case 'twitter':
        let shareUrl: string;
        shareUrl = params.url;
        if ((shareUrl.substring(0, 4).toLowerCase()) !== 'http') {
          shareUrl = 'https://' + shareUrl;
        }
        appParams = 'url=' + shareUrl + '&text=' + params.shortDescription + '&via=' + params.via;
        break;
      case 'linkedin':
        appParams = 'url=' + params.url;
        break;
    }
    return appParams;
  }

  trackSocialShare(app, additionalInfo) {
    this._ga.trackSocialShare(app, additionalInfo);
  }

  /* 
  *    Cookie processing
  */

  createCookie(data) {
    this._cookie.set('globals', JSON.stringify(data), undefined, '/');
  }

  getCookie() {
    return this._cookie.check('globals') ? JSON.parse(this._cookie.get('globals')) : null;
  }

  deleteCookie() {
    this._cookie.delete('globals', '/');
  }

  /* 
  *    Encode/Decode value
  */

  encodeData(value) {
    return Base64.encode(value);
  }

  decodeData(value) {
    return Base64.decode(value);
  }
}


/* 
*    Base64 encoding service
*/

var Base64 = {

  keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

  encode: function (input) {
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;

    do {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);

      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;

      if (isNaN(chr2)) {
        enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
        enc4 = 64;
      }

      output = output + this.keyStr.charAt(enc1) + this.keyStr.charAt(enc2) + this.keyStr.charAt(enc3) + this.keyStr.charAt(enc4);
      chr1 = chr2 = chr3 = "";
      enc1 = enc2 = enc3 = enc4 = "";
    } while (i < input.length);

    return output;
  },

  decode: function (input) {
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;

    // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
    var base64test = /[^A-Za-z0-9\+\/\=]/g;
    if (base64test.exec(input)) {
      window.alert("There were invalid base64 characters in the input text.\n" + "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" + "Expect errors in decoding.");
    }
    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

    do {
      enc1 = this.keyStr.indexOf(input.charAt(i++));
      enc2 = this.keyStr.indexOf(input.charAt(i++));
      enc3 = this.keyStr.indexOf(input.charAt(i++));
      enc4 = this.keyStr.indexOf(input.charAt(i++));

      chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;

      output = output + String.fromCharCode(chr1);

      if (enc3 != 64) {
        output = output + String.fromCharCode(chr2);
      }
      if (enc4 != 64) {
        output = output + String.fromCharCode(chr3);
      }

      chr1 = chr2 = chr3 = "";
      enc1 = enc2 = enc3 = enc4 = "";

    } while (i < input.length);

    return output;
  }
};
