import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { throwError, Observable, forkJoin } from 'rxjs';
import { HttpHelperService } from './http-helper.service';
import { HttpClient } from '@angular/common/http';
import { SearchListService } from './search-list.service';
import { RecentlyViewedService } from './recently-viewed.service';


@Injectable({
  providedIn: 'root'
})
export class CareerDetailService {
  mcId: any;
  allFilter: any;

  constructor(
    private _httpHelper: HttpHelperService,
    private _searchListService: SearchListService,
    private _route: ActivatedRoute,
    private _http: HttpClient,
    private _recentlyViewedService: RecentlyViewedService
  ) { }

  getWhatTheyDo(mcId): Observable<any> {
    const params = this.createMcIdParam(mcId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-what-they-do/', params));
  }

  getTrainingProvided(mcId): Observable<any> {

    const params = this.createMcIdParam(mcId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-training-provided/', params));
  }

  getRelatedCareer(mcId): Observable<any> {

    const params = this.createMcIdParam(mcId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-related-career/', params));
  }

  getRelatedCivilianCareer(mcId): Observable<any> {

    const params = this.createMcIdParam(mcId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-related-civilian-career/', params));
  }

  getMilitaryCareer(mcId): Observable<any> {

    const params = this.createMcIdParam(mcId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-military-career/', params));
  }

  getServiceOffering(mcId): Observable<any> {

    const params = this.createMcIdParam(mcId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-service-offering/', params));
  }

  getAdditionalInfo(mcId): Observable<any> {

    const params = this.createMcIdParam(mcId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-additional-info/', params));
  }

  getHotJobTitles(mcId): Observable<any> {

    const params = this.createMcIdParam(mcId);

    return this._http.get(this._httpHelper.getFullUrl('/CareerProfile/getHotJobTitle/', params));
  }

  getHelpfulAttributes(mcId): Observable<any> {

    const params = this.createMcIdParam(mcId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-helpful-attribute/', params));
  }

  getCareerPathway(mcId): Observable<any> {

    const params = this.createMcIdParam(mcId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-career-path/', params));
  }

  getSkillRating(mcId): Observable<any> {

    const params = this.createMcIdParam(mcId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-skill-rating/', params));
  }

  getParentSkillRating(pMcId): Observable<any> {

    const params = this.createMcIdParam(pMcId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-skill-rating/', params));
  }

  getParentMilitaryCareer(pMcId): Observable<any> {

    const params = this.createMcIdParam(pMcId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-military-career/', params));
  }

  getOtherTitles(mcId): Observable<any> {

    const params = this.createMcIdParam(mcId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-other-titles/', params));
  }

  getArmyScores(mcId): Observable<any> {

    const params = this.createMcIdParam(mcId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-see-score-army/', params));
  }

  getServiceOfferingCompositeScore(mcId): Observable<any> {

    const params = this.createMcIdParam(mcId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-other-titles/', params));
  }

  getFeaturedProfile(mcId): Observable<any> {

    const params = this.createMcIdParam(mcId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-featured-profiles/', params));
  }

  getAirForceDetail(mcId, mocId): Observable<any> {

    const params = this.createServiceParam(mcId, mocId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-air-force-detail/', params));
  }

  getArmyDetail(mcId, mocId): Observable<any> {

    const params = this.createServiceParam(mcId, mocId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-army-detail/', params));
  }

  getCoastGuardDetail(mcId, mocId): Observable<any> {

    const params = this.createServiceParam(mcId, mocId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-coast-guard-detail/', params));
  }

  getMarineDetail(mcId, mocId): Observable<any> {

    const params = this.createServiceParam(mcId, mocId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-marine-detail/', params));
  }

  getNavyDetail(mcId, mocId): Observable<any> {

    const params = this.createServiceParam(mcId, mocId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-navy-detail/', params));
  }

  getProfile(mcId): Observable<any> {
    const params = this.createMcIdParam(mcId);

    return this._http.get(this._httpHelper.getFullUrl('/detail-page/get-profile/', params));
  }

  createMcIdParam(mcId) {
    const params = [];
    params.push({
      key: null,
      value: mcId
    });
    return params;
  }

  createServiceParam(mcId, mocId) {
    const params = [];
    params.push({
      key: null,
      value: mcId
    });
    params.push({
      key: null,
      value: mocId
    });
    return params;
  }
}
