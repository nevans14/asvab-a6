import { Injectable } from '@angular/core';
import {  Observable } from 'rxjs';
import { HttpHelperService } from './http-helper.service';
import { UserService } from '../services/user.service';
import { ConfigService } from './config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

  const headers = new HttpHeaders()
  .set('Accept', 'application/json')
  .set('Content-Type', 'application/json');

@Injectable({
  providedIn: 'root'
})
export class SearchListService  {
  public searchListObject: any;
  public careerProfileListObject: any;
  public scoreListObject: any;
  public jobFamilyListObject: any;
  public currentSearchCriteria: any;
  hiddenListObject: any;
  processingSearchListObject: any;
  processingSearchList = false;
  refreshSearchListInterval: any;

  constructor(
    private _httpHelper: HttpHelperService,
    private _userService: UserService,
    private _congfigService: ConfigService,
    private _http: HttpClient
  ) { }


  getSearchListObsv(): Observable<any>  {
    return this._http.get(this._congfigService.getAwsFullUrl('/api/search/get-search-list'));
  }

  getCareerProfileList(): Observable<any>  {
    return this._http.get(this._congfigService.getFullUrl( '/CareerProfile/getCareerProfileList/', null));
  }

  getJobFamily (): Observable<any>  {

    return this._http.get(this._httpHelper.getFullUrl( 'search/get-job-family/' ,  null));
  }

  refreshSearchList() {
    const self = this;
    if (this.processingSearchList) {
      this.refreshSearchListInterval = setInterval(function () {
        console.log('Wait to process search list refresh.');
        if (!self.processingSearchList) {
          self.processRefreshSearchList();
          clearInterval(self.refreshSearchListInterval);
        }
      }, 500);
    } else {
      this.processRefreshSearchList();
    }
  }

  processRefreshSearchList() {
    this.processingSearchList = true;
    this._userService.favoritesListObject = null;
    this._userService.hiddenListObject = null;
    this.obtainSearchList(false);
  }

  getSearchList() {
    if (!this.searchListObject || this.processingSearchList) {
      if (!this.processingSearchList) {
        this.processingSearchList = true;
        this.obtainSearchList(true);
      }
      return null;
    } else {
      return this.searchListObject;
    }
  }

  obtainSearchList(getSearchList = true) {

    if (getSearchList) {
      this.getSearchListObsv().subscribe(
        data => {
          this.processingSearchListObject = data;
          this.processSearchList();
        });
    } else {
      this.processingSearchListObject = this.searchListObject;
      this.processSearchList();
    }

    if (this._congfigService.isLoggedIn()) {
      this._userService.getFavoritesList().subscribe(
        data => {
          this._userService.favoritesListObject = data ? data : [];
          this.processSearchList();
        });

      this._userService.getHiddenList().subscribe(
        data => {
          this._userService.hiddenListObject = data ? data : [];
          this.processSearchList();
        });
    } else {
      this._userService.favoritesListObject = [];
      this._userService.hiddenListObject = [];
      this.processSearchList();
    }

  }

  processSearchList() {
    if (this.processingSearchListObject != null &&
      this._userService.favoritesListObject != null &&
      this._userService.hiddenListObject != null) {
      const imageUrl = this._congfigService.getImageUrl();

      // This is for when we do an SSO redirect we need to finish the refresh before doing the redirect or iOS throws an error
      if (!this.processingSearchListObject) {
        this.processingSearchListObject = this.searchListObject;
      }

      for (let x = 0; x < this.processingSearchListObject.length; x++) {
        this.processingSearchListObject[x].show = true;
        this.processingSearchListObject[x].favorited = false;
        this.processingSearchListObject[x].imageUrl = imageUrl + 'citm-images/' + this.processingSearchListObject[x].mcId + '.jpg';
        this.processingSearchListObject[x].backgroundImageUrl =
          'url(\'' + imageUrl + 'citm-images/' + this.processingSearchListObject[x].mcId + '.jpg\')';
      }

      if (this._userService.hiddenListObject != null) {
        for (let i = 0; i < this._userService.hiddenListObject.length; i++) {
          const mcId = this._userService.hiddenListObject[i];
          for (let x = 0; x < this.processingSearchListObject.length; x++) {
            if (this.processingSearchListObject[x].mcId === mcId) {
              this.processingSearchListObject[x].show = false;
              break;
            }
          }
        }
      }

      if (this._userService.favoritesListObject != null) {
        for (let i = 0; i < this._userService.favoritesListObject.length; i++) {
          const mcId = this._userService.favoritesListObject[i].mcId;
          for (let x = 0; x < this.processingSearchListObject.length; x++) {
            if (this.processingSearchListObject[x].mcId === mcId) {
              this.processingSearchListObject[x].favorited = true;
              break;
            }
          }
        }
      }
      this.searchListObject = this.processingSearchListObject;
      this.processingSearchList = false;
      this.processingSearchListObject = null;
    }
  }

  getProcessingSearchList() {
    return this.processingSearchList;
  }
}
