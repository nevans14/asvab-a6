import { Injectable } from '@angular/core';
import { HttpHelperService } from 'app/services/http-helper.service';
import { SessionService } from 'app/services/session.service';
import { CookieService } from 'ngx-cookie-service';
import { UtilityService } from 'app/services/utility.service';
import { GoogleAnalyticsService } from '../services/google-analytics.service';

declare var bCrypt: any;

@Injectable( {
    providedIn: 'root'
} )
export class LoginService {
    readonly loginBaseUrl = 'applicationAccess/';
    restApiUrl: any;
    ssoApiUrl: any;

    constructor(
        private _httpHelper: HttpHelperService,
        private _sessionService: SessionService,
        private cookieService: CookieService,
        private _utilityService: UtilityService,
        private _googleAnalyticsService: GoogleAnalyticsService
    ) {
        this.restApiUrl = this._httpHelper.getApiUrl();
        this.restApiUrl = this.restApiUrl[this.restApiUrl.length - 1] === '/' ? this.restApiUrl : this.restApiUrl + '/';
        this.ssoApiUrl = this._httpHelper.getSsoUrl();
    }

    emailLogin( email, password ) {
        let data = {
            email: email,
            accessCode: "",
            password: password,
            resetKey: ""
        }

        return this._httpHelper.httpHelper( 'POST', this.loginBaseUrl + 'loginEmail', null, data )
            .then(( obj ) => {
                let retStatus: number = obj['statusNumber'];
                switch ( retStatus ) {

                    case ( 1002 ):
                    case ( 1008 ):
                        return 'Username / Password combination is invalid';

                    case ( 1010 ):
                        return 'Username has been locked, try resetting your password.';

                    case ( 0 ):
                        this.setCredentials( data['email'], obj['user_id'], obj['role'], true );
                        window.location.href =
                        this.restApiUrl  + 'sso/confirm_login?redirect=' + this.ssoApiUrl + 'login_callback/test';
                        this._googleAnalyticsService.trackStudent(obj);
                        return;

                    case ( 1 ):
                        this.setCredentials( data['email'], obj['user_id'], obj['role'], false );
                        this._googleAnalyticsService.trackStudent(obj);
                        return;

                    default:
                        const error = obj['status'];
                        return error.substring( error.indexOf( ":" ) + 1, error.length + 1 );
                }
            } );

    }

    emailRegistration( email, password, recaptchaResponse ) {
        var data = {
            email: email,
            accessCode: "",
            password: password,
            resetKey: "",
            recaptchaResponse: recaptchaResponse,

        };

        return new Promise(( resolve, reject ) => {

            // TODO: bcrypt should be done on server side.
            this.getCryptPW( password ).then(( hashRet ) => {
                data.password = hashRet;
                return this._httpHelper.httpHelper( 'POST', this.loginBaseUrl + 'register', null, data )
                    .then(( obj ) => {
                        let retStatus = obj['statusNumber'];
                        switch ( retStatus ) {

                            case ( 0 ):
                                this.setCredentials( data['email'], obj['user_id'], obj['role'] , true);
                                window.location.href =
                                this.restApiUrl + 'sso/confirm_login?redirect=' + this.ssoApiUrl + 'login_callback/test';
                                break;
                            case ( 1 ):
                                this.setCredentials( data['email'], obj['user_id'], obj['role'],  false);
                                break;
                            default:
                        }
                        resolve( obj );
                    } );
            } )
                .catch(( error ) => {
                    console.log( error );
                    reject( error );
                } );
        } );
    }

    getCryptPW( password ) {
        var promise = new Promise(( resolve, reject ) => {

            this.crypt( password, function( hashRet ) {
                resolve( hashRet );
            } );

        } );
        return promise;
    }

    passwordResetRequest( email ) {
        var data = {
            email: email,
            accessCode: "",
            password: "",
            resetKey: ""
        };

        return this._httpHelper.httpHelper( 'POST', this.loginBaseUrl + 'passwordResetRequest', null, data )
            .then(( obj ) => {
                return obj[status];
            } );
    }

    setCredentials( email, userId, role, sso ) {
        let now = new Date();
        let password = null;
        let addSso = 'null';
        if (sso) {
            addSso = 'SSO';
        }
        const authdata = this._utilityService.encodeData( userId + ':' + password + ':' + now.toISOString() + ':' + addSso );

        const globals = {
            currentUser: {
                role: role,
                authdata: authdata
            }
        };

        // Refresh search list.
        // create cookie
        this.cookieService.set( 'globals', JSON.stringify(globals), undefined, '/' );
        this._sessionService.resetSearchList();
    }

    //*** Begin ***
    id;
    bcrypt = new bCrypt();
    //bcypt;

    begin = '';
    hash;
    result( hashRet ) {
        console.log( hashRet );
        this.hash = hashRet;
    }

    ckpassword = function( password, hash, callbackFunction ) {
        try {

            this.bcrypt.checkpw( password, hash, callbackFunction, null );
        } catch ( err ) {
            alert( err );
            return;
        }
    };

    crypt = function( password, callbackFunction ) {
        var salt = '';
        var rounds = 10;

        // Auto generate the salt
        try {
            salt = this.bcrypt.gensalt( rounds );
        } catch ( err ) {
            alert( err );
            return;
        }

        try {
            // bcrypt.hashpw( password , salt, result , null );
            this.bcrypt.hashpw( password, salt, callbackFunction, null );
        } catch ( err ) {
            alert( err );
            return;
        }
    }
    // *** End ***
}
