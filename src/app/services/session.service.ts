import { Injectable } from '@angular/core';
import { Router, Resolve } from '@angular/router';
import { SearchListService } from 'app/services/search-list.service';
import { UserService } from 'app/services/user.service';
import { ConfigService } from 'app/services/config.service';
import { MatDialog } from '@angular/material';
import { MessageDialogComponent } from 'app/core/dialogs/message-dialog/message-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class SessionService implements Resolve<any>{
  private user: any;
  private userId: String;
  private refreshSearchListInterval: any;

  constructor(
    private _userService: UserService,
    private _searchService: SearchListService,
    private _config: ConfigService,
    private _dialog: MatDialog,
    private _router: Router
) { }

resolve() {
  return this.resetSearchList('', false);
}

logOff() {
  this._config.logOff();
  this.resetSearchList('/home');
}

sessionTimeOut() {
  this._dialog.closeAll();
  const data = {
    'title': 'Error',
    'message': 'Session Ended! Please log back in to continue what you were doing.'
  };
  const dialogRef1 = this._dialog.open(MessageDialogComponent, {
    data: data
  });
  this.logOff();
}

  resetSearchList(url = '', refreshPage = true) {
    this._searchService.refreshSearchList();
    let currentPath = this._router.url;
    if (refreshPage) {
      if (url !== '') {
        currentPath = url;
      }
      this._router.navigate([currentPath]);
    }
  }
}
