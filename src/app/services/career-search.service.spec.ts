import { TestBed } from '@angular/core/testing';

import { CareerSearchService } from './career-search.service';

describe('CareerSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CareerSearchService = TestBed.get(CareerSearchService);
    expect(service).toBeTruthy();
  });
});
