import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { SearchListService } from 'app/services/search-list.service';

@Injectable( {
    providedIn: 'root'
} )
export class GuidedExplorationResolveService implements Resolve<any> {

    constructor( private _searchService: SearchListService ) { }


    resolve() {
        const q = new Promise(( resolve, reject ) => {

            let searchObject = this._searchService.getSearchList();

            // Utilizing SearchListService. Other pages like guided exploration needs this data resolved before loading of page.
            const searchListInterval = setInterval(() => {
                if ( this._searchService.searchListObject ) {
                    clearInterval( searchListInterval );
                    resolve( this._searchService.searchListObject );
                }
            }, 200 );
        } );

        return q;
    }
}
