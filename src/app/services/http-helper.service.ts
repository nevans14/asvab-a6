import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { ConfigService } from '../services/config.service';

const headers = new HttpHeaders()
.set('Accept', 'application/json')
.set('Content-Type', 'application/json');

@Injectable({
  providedIn: 'root'
})
export class HttpHelperService implements Resolve<any> {
  restUrl: String;

  constructor(
    private _http: HttpClient,
    private _config: ConfigService
  ) {
      this.restUrl = this._config.getRestUrl();
  }

  resolve(route: ActivatedRouteSnapshot) {

    if (route.data.params.length < 3) {
      let x;
      for (x = 0; x < 3; x++) {
        if (x < 2) {
          route.data.params.push(null);
        } else {
          route.data.params.push(true);
        }
      }
    }
    return this.httpHelper(route.data.params[0], route.data.params[1], route.data.params[2], route.data.params[3]);
  }

  getFullUrl(url, params) {
    return this._config.getFullUrl(url, params);
  }

  public httpHelper(method: string, url: string, params, data: object) {
    return new Promise((resolve, reject) => {

      method = method.toUpperCase();
      let errorContext = null;

     const fullUrl = this.getFullUrl(url, params);

      if (method === 'GET') {
        // this._http.get(fullUrl, {observe: 'response', headers:  new HttpHeaders().set('Authorization',
        // 'Bearer ' + Cookie.get('access_token'))})
        this._http.get(fullUrl)
          .toPromise()
          .then(obj => {
//            console.log(obj);
            resolve(obj);
          })
          .catch(err => {
            errorContext = (err.error || 'Server error');
            reject(errorContext);
          });
      } else if (method === 'POST') {
        this._http.post(fullUrl, data)
          .toPromise()
          .then(obj => {
            console.log(obj);
            resolve(obj);
          })
          .catch(err => {
            errorContext = (err.error || 'Server error');
            reject(errorContext);
          });
      } else if (method === 'PUT') {
        this._http.put(fullUrl, data)
          .toPromise()
          .then(obj => {
            console.log(obj);
            resolve(obj);
          })
          .catch(err => {
            errorContext = (err.error || 'Server error');
            reject(errorContext);
          });
      } else if (method === 'DELETE') {
        this._http.delete(fullUrl)
          .toPromise()
          .then(obj => {
            console.log(obj);
            resolve(obj);
          })
          .catch(err => {
            errorContext = (err.error || 'Server error');
            reject(errorContext);
          });
      } else {
        errorContext = '***ERROR:  Method ' + method + ' not supported.';
        reject(errorContext);
      }
    });
  }

  public getFromUrl(url: string) {
    return new Promise((resolve, reject) => {
      this._http.get(url)
      .toPromise()
      .then(obj => {
        console.log(obj);
        resolve(obj);
      })
      .catch(err => {
        const errorContext = (err.error || 'Server error');
        reject(errorContext);
      });
    });
  }

  public getApiUrl() {
    return this._config.getRestUrl();
  }

  public getSsoUrl() {
    return this._config.getSsoUrl();
  }
}
