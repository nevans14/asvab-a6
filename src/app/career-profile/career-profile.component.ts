import { Component, OnInit, AfterViewInit, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfigService } from '../services/config.service';
import { CareerDetailService } from 'app/services/career-detail.service';
import { SearchListService } from '../services/search-list.service';
import { RecentlyViewedService } from '../services/recently-viewed.service';
import {GoogleAnalyticsService} from '../services/google-analytics.service';
import { UtilityService } from '../services/utility.service';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-career-profile',
  templateUrl: './career-profile.component.html',
  styleUrls: ['./career-profile.component.scss']
})
export class CareerProfileComponent implements OnInit, AfterViewInit {
  careerProfile;
  careerProfileList;
  searchList;
  careerDetails;
  militaryCareer;
  snapshotQuestionList = [];
  toMilitaryCareerList = [];
  jobQuestionList = [];
  customTitle;
  emailSubject;
  linkedInTitle;
  linkedInDescription;
  fullUrl: string;
  titles = null;
  mcId;
  showMore = true;
  showMore2 = true;
  showMore3 = true;
  showMore4 = true;
  slideConfig = {
    'slidesToShow': 1,
    'slidesToScroll': 1,
    'dots': false,
    'infinite': true,
    'autoplay': true,
    'autoplaySpeed': 2000,
    prevArrow: '<button type="button" class="slick-prev" role="button">' +
    '<img src="assets/images/arrow-left-red.png"><img src="assets/images/arrow-left-gray.png"></button>',
    nextArrow: '<button type="button" class="slick-next" role="button">' +
    '<img src="assets/images/arrow-right-red.png"><img src="assets/images/arrow-right-gray.png"></button>'
  };
  slideConfig2 = {
    'slidesToShow': 1,
    'slidesToScroll': 1,
    'dots': false,
    'infinite': true,
    'autoplay': false,
    prevArrow: '<button type="button" class="slick-prev" role="button">' +
    '<img src="assets/images/arrow-left-red.png"><img src="assets/images/arrow-left-gray.png"></button>',
    nextArrow: '<button type="button" class="slick-next" role="button">' +
    '<img src="assets/images/arrow-right-red.png"><img src="assets/images/arrow-right-gray.png"></button>'
  };

  slides:  any;
  profileSearchListInterval: any;

  constructor(
    private _route: ActivatedRoute,
    private _renderer2: Renderer2,
    private _config: ConfigService,
    private _careerDetailService: CareerDetailService,
    private _searchListService: SearchListService,
    private _meta: Meta,
    private _titleTag: Title,
    private _googleAnalyticsService: GoogleAnalyticsService,
    private _recentlyViewedService: RecentlyViewedService,
    private _utility: UtilityService,
    private _router: Router,
  ) {
    this._renderer2.addClass(document.body, 'expanded');
  }

  ngOnInit() {
    this._route.params.subscribe(res => this.mcId = res.mcId);

    this._careerDetailService.getMilitaryCareer(this.mcId).subscribe(careerData => {
      this.militaryCareer = careerData;
    });

    const searchObject = this._searchListService.getSearchList();
    const self = this;

    if (!searchObject) {
      if (!this.profileSearchListInterval) {
        this.profileSearchListInterval = setInterval(function () {
          if (!self._searchListService.getProcessingSearchList()) {
            clearInterval(self.profileSearchListInterval);
          }
        }, 200);
      }
    }

    if (!this._searchListService.careerProfileListObject) {
      this._searchListService.getCareerProfileList().subscribe(
        profileData => {
          this._searchListService.careerProfileListObject = profileData;
          this.careerProfileList = profileData;
          this.setProfileData();
        });
    } else {
      this.careerProfileList = this._searchListService.careerProfileListObject;
      this.setProfileData();
    }
  }

  ngAfterViewInit() {
    window.scrollTo( 0, 0 );
 }

  setProfileData() {
    const fullUrl = location.href;

    if (this.careerProfileList) {
      // Did this in a for loop as heard it processes faster that way
      for (let i = 0; i < this.careerProfileList.length; i++) {
        if (this.careerProfileList[i].mcId === this.mcId) {
          this.careerProfile = this.careerProfileList[i];
          break;
        }
      }

      this.snapshotQuestionList = this.careerProfile.snapshotQuestionList;
      this.toMilitaryCareerList = this.careerProfile.toMilitaryCareerList;
      this.jobQuestionList = this.careerProfile.jobQuestionList;
      this.customTitle = this.toMilitaryCareerList[0].customTitle;
      this.emailSubject = 'I found this job profile at Careers in the Military:  ' + this.customTitle;
      this.slides = this.careerProfile.imageList;

      const imageBaseUrl = this._config.getImageUrl();
      for (let x = 0; x < this.slides.length; x++) {
        this.slides[x].imageUrl = imageBaseUrl + 'profile-images/' + this.careerProfile.imageList[x].imageName;
      }
      for (let x = 0; x < this.toMilitaryCareerList.length; x++) {
        const title = this.toMilitaryCareerList[x].title;
        this.titles = this.titles ? this.titles + ', ' + title : title;
      }
      this._titleTag.setTitle(this.customTitle  + ' | Life in the Military' );
      this._meta.updateTag({name: 'description', content: this.careerProfile.teaserText});

      this._meta.updateTag({ property: 'og:title', content: this.customTitle  + ' | Life in the Military'  });
      this._meta.updateTag({ property: 'og:description', content: this.careerProfile.teaserText });
      this._meta.updateTag({ property: 'og:image', content: this.slides[0].imageUrl });
      this._meta.updateTag({ property: 'og:image:alt', content: this.titles });
      this._meta.updateTag({ property: 'og:url', content: fullUrl });

      this._meta.updateTag({ name: 'twitter:site', content: '@CareersintheMil'});
      this._meta.updateTag({ name: 'twitter:via', content: 'CareersintheMil'});
      this._meta.updateTag({ name: 'twitter:card', content: 'summary_large_image'});
      this._meta.updateTag({ name: 'twitter:title', content:  this.customTitle  + ' | Life in the Military'  })
      this._meta.updateTag({ name: 'twitter:image', content: this.slides[0].imageUrl });
      this._meta.updateTag({ name: 'twitter:description', content: this.careerProfile.teaserText});
    }
  }

  onSocialShareClick(app) {
    const params = {
      url: this.getShareUrl(),
      title: this.militaryCareer.title + ' | Life in the Military',
      description: this.careerProfile.teaserText,
      shortDescription: this.getShortenDescription(),
      image: this.slides[0].imageUrl,
      via: 'CareersintheMil'
    };
    this._utility.displaySocialShare(app, params, 'CITM CareerProfile');
  }

  // Need this at least for email
  trackSocialShare(app) {
    this._utility.trackSocialShare(app, 'CITM CareerProfile');
  }

  getShortenDescription() {
    return this.careerProfile.teaserText.substr(0, 200) + '...';
  }

  getShareUrl() {
    return window.location.href;
  }

  getTitle() {
    return this.emailSubject;
  }

  getDescription() {
    return this.emailSubject;
  }
}
